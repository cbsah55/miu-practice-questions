package com.cbs;

/**
 * A Madhav array has the following property.
 * a[0] = a[1] + a[2] = a[3] + a[4] + a[5] = a[6] + a[7] + a[8] + a[9] = ...
 * The length of a Madhav array must be n*(n+1)/2 for some n.
 * <p>
 * Write a method named isMadhavArray that returns 1 if its array argument is a Madhav array,
 * otherwise it returns 0. If you are programming in Java or C# the function signature is
 * int isMadhavArray(int[ ] a)
 */

public class ArrayManipulation {

    public static void main ( String[] args ) {
        int[] arr = {2, 1, 1};
        int[] arr1 = {2, 1, 1, 4, -1, -1};
        int[] arr2 = {6, 2, 4, 2, 2, 2, 1, 5, 0, 0};
        int[] arr3 = {18, 9, 10, 6, 6, 6};
        int[] arr4 = {-6, -3, -3, 8, -5, -4};
        int[] arr5 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, -2, -1};
        int[] arr6 = {3, 1, 2, 3, 0};

//        System.out.println(isMadhavArrayGit(arr));
//        System.out.println(isMadhavArrayGit(arr1));
        System.out.println(isMadhavArrayGit(arr2));
//        System.out.println(isMadhavArrayGit(arr3));
//        System.out.println(isMadhavArrayGit(arr4));
//        System.out.println(isMadhavArrayGit(arr5));
//        System.out.println(isMadhavArrayGit(arr6));
    }

    static int isMadhavArrayGit(int[] a) {
        int isMadhav = 1;
        int isMadhavLength = 0;

        if (a.length < 3) return 0;

        for (int i = 1; i <= a.length; i++) {    // check the length for each n, i.e each array index
            if (a.length == i*(i+1)/2) {
                isMadhavLength = 1;
            }
        }
        if (isMadhavLength == 0) return 0;

        int startIndex = 1;
        int round = 1;
        int endIndex = startIndex + round;
        int firstValue = a[0];

        while (isMadhav == 1 && endIndex <= a.length) {
            int sum = 0;
            for (int i = startIndex; i <= endIndex; i++) {   // sum each group
                sum += a[i];
            }
            if (firstValue != sum)
                isMadhav = 0;

            round++;
            startIndex = endIndex + 1;
            endIndex = startIndex + round;  //increase the end index by round, i.e. 1st round -> start = 1 , end = 2; 2nd round -> start = 3, end = 5;
        }
        return isMadhav;
    }



}
