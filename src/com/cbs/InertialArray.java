package com.cbs;

import java.util.ArrayList;
import java.util.List;

/**
 * . An array is defined to be inertial if the following conditions hold:
 * a. it contains at least one odd value
 * b. the maximum value in the array is even
 * c. every odd value is greater than every even value that is not the maximum value.
 * So {11, 4, 20, 9, 2, 8} is inertial because
 * a. it contains at least one odd value
 * b. the maximum value in the array is 20 which is even
 * c. the two odd values (11 and 9) are greater than all the even values that are not equal to 20 (the maximum), i.e., (4, 2, 8}.
 * However, {12, 11, 4, 9, 2, 3, 10} is not inertial because it fails condition (c), i.e.,
 * 10 (which is even) is greater 9 (which is odd) but 10 is not the maximum value in the array.
 * <p>
 * Write a function called isIntertial that accepts an integer array and returns 1 if the array is inertial; otherwise it returns 0.
 *
 * @Author
 * @Created 2024/01/08
 * @Name MIU
 */

public class InertialArray {

    public static void main ( String[] args ) {
        int[] arr = {1};
        int[] arr1 = {2};
        int[] arr2 = {1, 2, 3, 4};
        int[] arr3 = {1, 1, 1, 1, 1, 1, 2};
        int[] arr4 = {2, 12, 4, 6, 8, 11};
        int[] arr5 = {2, 12, 12, 4, 6, 8, 11};
        int[] arr6 = {-2, -4, -6, -8, -11};
        int[] arr7 = {2, 3, 5, 7};
        int[] arr8 = {2, 4, 6, 8, 10};

        System.out.println(isInertial(arr));
        System.out.println(isInertial(arr1));
        System.out.println(isInertial(arr2));
        System.out.println(isInertial(arr3));
        System.out.println(isInertial(arr4));
        System.out.println(isInertial(arr5));
        System.out.println(isInertial(arr6));
        System.out.println(isInertial(arr7));
        System.out.println(isInertial(arr8));
        System.out.println("#############################");
        System.out.println(isInertialV2(arr));
        System.out.println(isInertialV2(arr1));
        System.out.println(isInertialV2(arr2));
        System.out.println(isInertialV2(arr3));
        System.out.println(isInertialV2(arr4));
        System.out.println(isInertialV2(arr5));
        System.out.println(isInertialV2(arr6));
        System.out.println(isInertialV2(arr7));
        System.out.println(isInertialV2(arr8));

    }

    static int isInertial ( int[] arr ) {
        List<Integer> oddList = new ArrayList<>();
        List<Integer> evenList = new ArrayList<>();
        Integer maxValue = Integer.MIN_VALUE;

        for ( int i = 0; i < arr.length; i++ ) {

            if ( arr[i] % 2 == 0 ) {
                evenList.add(arr[i]);
            } else {
                oddList.add(arr[i]);
            }

            if ( arr[i] > maxValue ) {
                maxValue = arr[i];
            }

        }

        if ( oddList.isEmpty() ) {
            return 0;
        }

        if ( maxValue % 2 != 0 ) {
            return 0;
        }

        evenList.removeAll(List.of(maxValue));

        for ( int oddNumber :
                oddList ) {
            for ( int evenNumber :
                    evenList ) {
                if ( oddNumber < evenNumber ) {
                    return 0;
                }
            }
        }


        return 1;
    }


    static int isInertialV2(int[] arr){
        int evenLength = 0;
        int oddLength = 0;
        int maxValue = Integer.MIN_VALUE;

        for ( int i = 0; i < arr.length; i++ ) {
            if ( arr[i] % 2 == 0 || arr[i] % 2 == -0 ){
                evenLength++;
            }else {
                oddLength++;
            }

            if ( arr[i] > maxValue ){
                maxValue = arr[i];
            }
        }

        if ( oddLength <= 0 ){
            return 0;
        }

        if ( maxValue % 2 != 0 || maxValue % 2 != -0 ){
            return  0;
        }

        int[] oddValues = new int[oddLength];
        int[] evenValues = new int[evenLength];
        int oddCounter = 0;
        int evenCounter = 0;

        for ( int i = 0; i < arr.length; i++ ) {
            if ( arr[i] % 2 == 0 || arr[i] % 2 == -0  ){
                if ( arr[i] < maxValue ) {
                    evenValues[evenCounter] = arr[i];
                    evenCounter++;
                }
            }else {
                oddValues[oddCounter] = arr[i];
                oddCounter++;
            }
        }

        for ( int i = 0; i < oddValues.length; i++ ) {
            for ( int j = 0; j < evenValues.length; j++ ) {
                if ( oddValues[i] < evenValues[j]  ){
                    return 0;
                }
            }
        }


        return 1;
    }

}
