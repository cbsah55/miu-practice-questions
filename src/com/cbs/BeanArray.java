package com.cbs;

/**
 * An array is defined to be a Bean array if it meets the following conditions
 * a. If it contains a 9 then it also contains a 13.
 * b. If it contains a 7 then it does not contain a 16.
 * So {1, 2, 3, 9, 6, 13}  and {3, 4, 6, 7, 13, 15}, {1, 2, 3, 4, 10, 11, 12} and {3, 6, 9, 5, 7, 13, 6, 17} are  Bean arrays.
 * The following arrays are not Bean arrays:
 * a. { 9, 6, 18} (contains a 9 but no 13)  b. {4, 7, 16} (contains both a 7 and a 16)
 * Write a function named isBean that returns 1 if its array argument is a Bean array, otherwise it returns  0.
 * If you are programming in Java or C#, the function signature is  int isBean (int[ ] a)
 * If you are programming in C or C++, the function signature is  int isBean (int a[ ], int len) where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/01/24
 * @Name MIU
 */

public class BeanArray {

    public static void main ( String[] args ) {
        int[] arr = {1, 2, 3, 9, 6, 13};
        int[] arr1 = {3, 4, 6, 7, 13, 15};
        int[] arr2 = {1, 2, 3, 4, 10, 11, 12};
        int[] arr3 = {3, 6, 9, 5, 7, 13, 6, 17};
        int[] arr4 = { 9, 6, 18};
        int[] arr5 = {4, 7, 16};

        System.out.println(isBean(arr));
        System.out.println(isBean(arr1));
        System.out.println(isBean(arr2));
        System.out.println(isBean(arr3));
        System.out.println(isBean(arr4));
        System.out.println(isBean(arr5));
    }

    private static int isBean ( int[] arr ) {
        boolean hasDigitNine = false;
        boolean hasDigitThirteen = false;
        boolean hasDigitSeven = false;
        boolean hasDigitSixteen = false;
        for ( int i = 0; i < arr.length; i++ ) {
            if ( arr[i] == 9 ){
                hasDigitNine=true;
            }  else if(arr[i]==13){
                hasDigitThirteen = true;
            }  else if(arr[i]==7){
                hasDigitSeven = true;
            }    else if(arr[i]==16){
                hasDigitSixteen = true;
            }
        }

        return (hasDigitNine && hasDigitThirteen) || (hasDigitSeven && !hasDigitSixteen) ? 1 : 0;
    }
}
