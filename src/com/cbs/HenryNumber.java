package com.cbs;

/**
 * A perfect number is one that is the sum of its factors, excluding itself.
 * The 1st perfect number is 6 because 6 = 1 + 2 + 3.
 * The 2nd perfect number is 28 which equals 1 + 2 + 4 + 7 + 14.
 * The third is 496 = 1 + 2 + 4 + 8 + 16 + 31 + 62 + 124 + 248.
 * In each case, the number is the sum of all its factors excluding itself.
 * Write a method named henry that takes two integer arguments, i and j and returns the sum of the ith and jth perfect numbers.
 * So for example, henry (1, 3) should return 502 because 6 is the 1st perfect number and 496 is the 3rd perfect number and 6 + 496 = 502.
 * The function signature is
 * int henry (int i, int j)
 * You do not have to worry about integer overflow,
 * i.e., you may assume that each sum that you have to compute can be represented as a 31 bit integer.
 * Hint: use modulo arithmetic to determine if one number is a factor of another.
 *
 * @Author
 * @Created 2024/01/12
 * @Name MIU
 */

public class HenryNumber {

    public static void main ( String[] args ) {
        System.out.println(henry(1,3));
        System.out.println(henryV2(1, 1));  // should print 12 (6 + 6)
        System.out.println(henryV2(2, 2));  // should print 56 (28 + 28)
        System.out.println(henryV2(1, 2));  // should print 34 (6 + 28)
        System.out.println(henryV2(2, 3));  // should print 524 (28 + 496)
        System.out.println(henryV2(3, 3));  // should print 992 (496 + 496)
        System.out.println(henryV2(1,3));   // should print 502 ( 6 + 496)

    }



    private static int henry ( int firstNumber, int secondNumber ) {
        int count = 0;
        int number =2;

        int sumOfPerfectNumber = 0;

        while ( count < secondNumber  ){
            int sumOfFactor = 0;
            for ( int i = 1; i < number; i++ ) {
                if ( isFactor(number,i) ){
                    sumOfFactor += i;
                }
            }
            if ( sumOfFactor == number ){
                count++;
            }
            if ( count == firstNumber || count == secondNumber ) {
                sumOfPerfectNumber += sumOfFactor;
                firstNumber=0;
            }
            number++;


        }

        return sumOfPerfectNumber;
    }


    private static boolean isFactor(int nummber, int divisor) {
          return (nummber % divisor) == 0;
    }


    public static int henryV2(int i, int j){
        int iThNumber = getIthNumber(i);
        int jThNumber = getIthNumber(j);
        return iThNumber + jThNumber;
    }

    private static int getIthNumber ( int i ) {
        int count = 0;
        int num = 1;
        while ( true ){
            if ( isPerfect(num) ){
                count++;
                if ( count == i ){
                    return num;
                }
            }
            num++;
        }

    }



    private static boolean isPerfect(int num){
        int sum = 0;

        for ( int i = 1; i <= num / 2; i++ ) {
            if ( num % i == 0 ){
                sum += i;
            }
        }

        return sum == num;
    }

}
