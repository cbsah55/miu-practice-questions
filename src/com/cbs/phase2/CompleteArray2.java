package com.cbs.phase2;

/**
 * An array is defined to be complete if all its elements are greater than 0 and all even numbers
 * that  are less than the maximum even number are in the array.
 * For example {2, 3, 2, 4, 11, 6, 10, 9, 8} is complete because
 * a. all its elements are greater than 0
 * b. the maximum even integer is 10
 * c. all even numbers that are less than 10 (2, 4, 6, 8) are in the array.
 * But {2, 3, 3, 6} is not complete because the even number 4 is missing.
 * {2, -3, 4, 3, 6} is not complete  because it contains a negative number.
 * Write a function named isComplete that returns 1 if its array argument is a complete array. Otherwise  it returns 0.
 * The function signature is     int isComplete (int[ ] a)
 *
 * @Author
 * @Created 2024/04/01
 * @Name MIU
 */

public class CompleteArray2 {

    public static void main ( String[] args ) {
        int[] arr1 = {2, 3, 2, 4, 11, 6, 10, 9, 8};
        int[] arr2 = {2, 3, 3, 6};
        int[] arr3 = {2, -3, 4, 3, 6};

        System.out.println(isComplete(arr1));
        System.out.println(isComplete(arr2));
        System.out.println(isComplete(arr3));
    }

    private static int isComplete ( int[] arr1 ) {
        int maxEven = Integer.MIN_VALUE;
        boolean isComplete = false;

        for ( int i = 0; i < arr1.length; i++ ) {
            if ( arr1[i] <=0 ){
                return 0;
            }

            if ( arr1[i] % 2 == 0 && arr1[i] > maxEven ){
                maxEven = arr1[i] ;
            }
        }

        for ( int i = 2; i < maxEven; i+=2 ) {
            for ( int j = 0; j < arr1.length; j++ ) {
                if ( arr1[j] % 2 ==0 && arr1[j] == i ){
                    isComplete = true;
                    break;
                } else {
                    isComplete = false;
                }

                if ( j ==  arr1.length-1  &&  !isComplete){
                    return 0;
                }
            }

        }


        return isComplete ? 1 : 0;

    }
}
