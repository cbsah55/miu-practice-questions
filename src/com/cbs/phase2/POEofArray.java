package com.cbs.phase2;

/**
 * Consider an array A with n of positive integers. An integer idx is called a POE (point of equilibrium) of A,
 * if A[0]+A[1]+...+A[idx-1] is equal to A[idx+1]+A[idx+2]+...+A[n-1].
 * Write a function to return POE of an array, if it exists and -1 otherwise.
 * The signature of the function is: int f(int[] a)
 * Examples
 * -------------------------|--------------------------------------------------------------
 * | if the input arrays are | return                                                       |
 * |-------------------------|--------------------------------------------------------------|
 * | {1,8,3,7,10,2}          | 3 Reason: a[0]+a[1]+a[2] is equal to a[4]+a[5]               |
 * |-------------------------|--------------------------------------------------------------|
 * | {1,5,3,1,1,1,1,1,1}     | 2 Reason: a[0]+a[1] is equal to a[3]+a[4]+a[5]+a[6]+a[7]+a[8]|
 * |-------------------------|--------------------------------------------------------------|
 * | {2,1,1,1,2,1,7}         | 5 Reason: a[0]+a[1]+a[2]+a[3]+a[4] is equal to a[6]          |
 * |-------------------------|--------------------------------------------------------------|
 * | {1,2,3}                 | -1 Reason: No POE                                            |
 * |-------------------------|--------------------------------------------------------------|
 * | {3,4,5,10}              | -1 Reason: No POE                                            |
 * |-------------------------|--------------------------------------------------------------|
 * | {1,2,10,3,4}            | -1 Reason: No POE                                            |
 * -------------------------|--------------------------------------------------------------
 *
 * @Author
 * @Created 2024/04/05
 * @Name MIU
 */

public class POEofArray {

    public static void main ( String[] args ) {
        int[] arr = {1,8,3,7,10,2};
        int[] arr1 = {1,5,3,1,1,1,1,1,1};
        int[] arr2 = {2,1,1,1,2,1,7} ;
        int[] arr3 = {1,2,3} ;
        int[] arr4 = {3,4,5,10};
        int[] arr5 = {1,2,10,3,4} ;

        System.out.println(getPOE(arr));
        System.out.println(getPOE(arr1));
        System.out.println(getPOE(arr2));
        System.out.println(getPOE(arr3));
        System.out.println(getPOE(arr4));
        System.out.println(getPOE(arr5));
    }

    private static int getPOE ( int[] arr ) {

        for ( int i = 1; i < arr.length; i++ ) {
           int leftIndex = i;
           int rightIndex = i + 1;
            int leftSum = 0;
            int rightSum = 0;
            for ( int j = 0; j < leftIndex; j++ ) {
               leftSum += arr[j];
            }

            for ( int j = rightIndex; j < arr.length; j++ ) {
                 rightSum += arr[j];
            }

            if ( leftSum == rightSum ){
                return i;
            }


        }

        return  -1;
    }
}
