package com.cbs.phase2;

/**
 * Define an array to be a 121 array if all its elements are either 1 or 2 and it begins with one or
 * more 1s followed by a one or more 2s  and then ends with the same number of 1s that it begins with.
 * Write a method named is121Array that returns 1 if its array argument is  a 121 array, otherwise, it returns 0.
 * If you are programming in Java or C#, the function signature is  int is121Array(int[ ] a)
 * If you are programming in C or C++, the function signature is  int is121Array(int a[ ], int len) where len is the number of elements in the array a.
 *
 * @Author
 * @Created 2024/04/03
 * @Name MIU
 */

public class OneTwoOneArray {

    public static void main ( String[] args ) {
        int[] arr1 = {1, 2, 1};
        int[] arr2 = {1, 1, 2, 2, 2, 1, 1};
        int[] arr3 = {1, 1, 2, 2, 2, 1, 1, 1};
        int[] arr4 = {1, 1, 2, 1, 2, 1, 1};
        int[] arr5 = {1, 1, 1, 2, 2, 2, 1, 1, 1, 3};
        int[] arr6 = {1, 1, 1, 1, 1, 1};
        int[] arr7 = {2, 2, 2, 1, 1, 1, 2, 2, 2, 1, 1} ;
        int[] arr8 = {1, 1, 1, 2, 2, 2, 1, 1, 2, 2};
        int[] arr9 = {2, 2, 2};

        System.out.println(is121Array(arr1));
        System.out.println(is121Array(arr2));
        System.out.println(is121Array(arr3));
        System.out.println(is121Array(arr4));
        System.out.println(is121Array(arr5));
        System.out.println(is121Array(arr6));
        System.out.println(is121Array(arr7));
        System.out.println(is121Array(arr8));
        System.out.println(is121Array(arr9));
    }

    private static int is121Array ( int[] array ) {
        boolean hasTwo = false;
        boolean hasOne = false;
        int leftCount =0, rightCount = 0;

        if ( array[array.length /2] !=2 ){
            return 0;
        }
        for ( int i = 0; i < array.length; i++ ) {
            if ( array[i] > 2 ){
                return 0;
            }
            if ( array[i] == 1 ){
                hasOne=true;
                leftCount++;
            }
            if ( array[i] ==2 ){
                hasTwo = true;
                break;
            }
        }


        for ( int i = array.length-1; i>=0; i-- ) {
            if ( array[i] > 2 ){
                return 0;
            }
            if ( array[i] == 1 ){
                hasOne= true;
                rightCount++;
            }
            if ( array[i] ==2 ){
                hasTwo = true;
                break;
            }
        }



        return hasOne && hasTwo && (leftCount == rightCount) ? 1 : 0;
    }
}
