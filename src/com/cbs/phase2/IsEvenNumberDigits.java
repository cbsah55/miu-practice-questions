package com.cbs.phase2;

/**
 * An Evens number is an integer whose digits are all even. For example 2426 is an Evens number  but 3224 is not.
 * Write a function named isEvens that returns 1 if its integer argument is an Evens number otherwise it  returns 0.
 * The function signature is  int isEvens (int n)
 *
 * @Author
 * @Created 2024/03/20
 * @Name MIU
 */

public class IsEvenNumberDigits {

    public static void main ( String[] args ) {
        System.out.println(isEvens(2426));
        System.out.println(isEvens(3224));
    }

    private static int isEvens ( int n ) {
        int isEven = 0;
        if ( n <=0 ){
            return isEven;
        }

        while ( n > 0 ){
            int digit = n % 10;
            if ( isEven(digit) ) {
                isEven =1;
            } else {
                isEven = 0;
            }

            n = n/10;

        }



        return isEven;
    }

    private static boolean isEven(int n){
        return n % 2 ==0;
    }

}
