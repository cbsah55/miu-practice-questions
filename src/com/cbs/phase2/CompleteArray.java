package com.cbs.phase2;


/**
 *
 * An array is defined to be complete if the conditions (a), (d) and (e) below hold.
 * a. The array contains even numbers
 * b. Let min be the smallest even number in the array.
 * c. Let max be the largest even number in the array.
 * d. min does not equal max
 * e. All numbers between min and max are in the array
   For example {-5, 6, 2, 3, 2, 4, 5, 11, 8, 7} is complete because
 * a. The array contains even numbers
 * b. 2 is the smallest even number
 * c. 8 is the largest even number
 * d. 2 does not equal 8
 * e. the numbers 3, 4, 5, 6, 7 are in the array.
 * Examples of arrays that are not complete are:
 * {5, 7, 9, 13} condition (a) does not hold, there are no even numbers.
 * {2, 2} condition (d) does not hold
 * {2, 6, 3, 4} condition (e) does not hold (5 is missing)
 *
 *  @Author
 *  @Created 2024/03/20
 *  @Name MIU
 *
*/



public class CompleteArray {

    public static void main ( String[] args ) {
        int[] arr1 = {-5, 6, 2, 3, 2, 4, 5, 11, 8, 7};
        int[] arr2 = {5, 7, 9, 13};
        int[] arr3 = {2, 2};
        int[] arr4 = {2, 6, 3, 4};

        System.out.println(isComplete(arr1));
        System.out.println(isComplete(arr2));
        System.out.println(isComplete(arr3));
        System.out.println(isComplete(arr4));

    }

    private static int isComplete ( int[] array ) {
        boolean hasEven = false;
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        boolean condE = false;

        for ( int i = 0; i < array.length; i++ ) {
             if ( isEven(array[i])) {
                 hasEven = true;
            }

             if ( array[i] < min  && isEven(array[i])){
                 min = array[i];
             }

            if ( array[i] > max && isEven(array[i])){
                max = array[i];
            }
        }

        for ( int i = min+1; i < max; i++ ) {
            for ( int j = 0; j < array.length; j++ ) {
                if ( array[j] == i ){
                    condE = true;
                } else {
                    condE = false;
                }
            }
        }

        return hasEven && isEven(min) && isEven(max) && min !=max &&  condE ? 1 : 0;
    }

    private static boolean isEven(int n){
        return n %2 ==0;
    }
}
