package com.cbs.phase2;

/**
 * A fancy number is a number in the sequence 1, 1, 5, 17, 61, … .Note that first two fancy numbers are 1 and any fancy
 * number other than the first two is sum of the three times previous one and two times the one before that.
 * See below:   1,  1,  3*1 +2*1 = 5  3*5 +2*1 = 17  3*17 + 2*5 = 61
 * Write a function named isFancy that returns 1 if its integer argument is a Fancy number, otherwise it returns 0.
 * The signature of the function is  int isFancy(int n)
 *
 * @Author
 * @Created 2024/04/01
 * @Name MIU
 */

public class FancyNumber {

    public static void main ( String[] args ) {
        System.out.println(isFancy(5));
        System.out.println(isFancy(1));
        System.out.println(isFancy(61));
        System.out.println(isFancy(17));
        System.out.println(isFancy(15));
        System.out.println(isFancy(4));
    }

    private static int isFancy ( int num ) {
        int a = 1;
        int b = 1;

        while ( b < num ){
            int temp = b ;
            b = 3 * b + 2 * a;
            a = temp;
        }

       return b == num ? 1 : 0;
    }

}
