package com.cbs.phase2;

/**
 *
 *
 * An array is defined to be a Magic array if the sum of the primes in the array is equal to the first
 * element of the array. If there are no primes in the array, the first element must be 0. So {21, 3, 7, 9,
 * 11, 4, 6} is a Magic array because 3, 7, 11 are the primes in the array and they sum to 21 which is the
 * first element of the array. {13, 4, 4, 4, 4} is also a Magic array because the sum of the primes is 13
 * which is also the first element. Other Magic arrays are {10, 5, 5}, {0, 6, 8, 20} and {3}. {8, 5, -5, 5, 3} is
 * not a Magic array because the sum of the primes is 5+5+3 = 13. Note that -5 is not a prime because
 * prime numbers are positive.
 * @Author
 * @Created 2024/03/20
 * @Name MIU
 */

public class MagicArray {

    public static void main ( String[] args ) {
        int[] arr1 = {21, 3, 7, 9, 11, 4, 6};
        int[] arr2 = {13, 4, 4, 4, 4};
        int[] arr3 = {10, 5, 5};
        int[] arr4 = {0, 6, 8, 20};
        int[] arr5 = {3};
        int[] arr6 = {8, 5, -5, 5, 3};

        System.out.println(isMagicArray(arr1));
        System.out.println(isMagicArray(arr2));
        System.out.println(isMagicArray(arr3));
        System.out.println(isMagicArray(arr4));
        System.out.println(isMagicArray(arr5));
        System.out.println(isMagicArray(arr6));
    }

    private static int isMagicArray ( int[] array ) {
         int sum = 0;
         boolean hasPrime = false;
        for ( int i = 0; i < array.length; i++ ) {
             if ( isPrime(array[i]) ){
                 sum += array[i];
                 hasPrime = true;
             }
        }

        return sum == array[0] || (!hasPrime && array[0] == 0) ? 1 : 0;
    }



    public static boolean isPrime(int n){
        if ( n<=1 )
            return false;

        for ( int i = 2; i < n; i++ ) {
            if ( n % i == 0 ){
                return false;
            }
        }

        return true;
    }

}
