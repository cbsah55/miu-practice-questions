package com.cbs.phase2;

/**
 * A Bean array is defined to be an array where for every value n in the array,
 * there is also an element n-1 or n+1 in the  array.
 * For example, {2, 10, 9, 3} is a Bean array because  2 = 3-1  10 = 9+1  3 = 2 + 1  9 = 10 -1
 * Other Bean arrays include {2, 2, 3, 3, 3}, {1, 1, 1, 2, 1, 1} and {0, -1, 1}.
 * The array {3, 4, 5, 7} is not a Bean array because of the value 7 which requires that the array
 * contains either the value 6  (7-1) or 8 (7+1) but neither of these values are in the array.
 * Write a function named isBean that returns 1 if its array argument is a Bean array.
 * Otherwise it returns a 0.
 * If you are programming in Java or C#, the function signature is  int isBean(int[ ]
 * a)  If you are programming in C or C++, the function signature is  int isBean(int a[ ], int len) where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/04/01
 * @Name MIU
 */

public class BeanArray2 {

    public static void main ( String[] args ) {
        int[] arr1 = {2, 10, 9, 3};
        int[] arr2 = {2, 2, 3, 3, 3};
        int[] arr3 = {1, 1, 1, 2, 1, 1};
        int[] arr4 = {0, -1, 1};
        int[] arr5 = {3, 4, 5, 7};

        System.out.println(isBean(arr1));
        System.out.println(isBean(arr2));
        System.out.println(isBean(arr3));
        System.out.println(isBean(arr4));
        System.out.println(isBean(arr5));
    }

    private static int isBean ( int[] array ) {
        int isBean = 0;
        for ( int i = 0; i < array.length; i++ ) {
            int nPlus = array[i] +1;
            int nMinus = array[i] -1;
            for ( int j = 0; j < array.length; j++ ) {
               if ( array[j] == nPlus || array[j] == nMinus ){
                   isBean = 1;
                   break;
               } else {
                   isBean = 0;
               }
            }
            if ( isBean == 0 ){
                return 0;
            }
        }

        return  isBean;
    }

}
