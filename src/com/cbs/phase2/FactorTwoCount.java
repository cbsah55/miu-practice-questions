package com.cbs.phase2;

/**
 * Write a function named factorTwoCount that returns the number of times that 2 divides the  argument.
 * For example, factorTwoCount(48) returns 4 because   48/2 = 24  24/2 = 12  12/2 = 6  6/2 = 3  2 does not divide 3 evenly.
 * Another example: factorTwoCount(27) returns 0 because 2 does not divide 27.
 * The function signature is     int factorTwoCount(int n);
 *
 * @Author
 * @Created 2024/04/02
 * @Name MIU
 */

public class FactorTwoCount {

    public static void main ( String[] args ) {
        System.out.println(factorTwoCount(48));
        System.out.println(factorTwoCount(27));
    }

    private static int factorTwoCount ( int num ) {
        int count = 0;

        while ( num % 2 == 0 ){
           count ++;
           num /= 2;
        }

        return count;
    }
}
