package com.cbs.phase2;

/**
 * Write a function named minDistance that returns the smallest distance between two factors of a  number.
 * For example, consider 13013 = 1*7*11*13. Its factors are 1, 7, 11, 13 and 13013.
 * minDistance(13013) would return 2 because the smallest distance between any two factors is 2 (13 -  11 = 2).
 * As another example, minDistance (8) would return 1 because the factors of 8 are 1, 2, 4, 8
 * and the smallest distance between any two factors is 1 (2 – 1 = 1).
 * The function signature is  int minDistance(int n)
 *
 * @Author
 * @Created 2024/03/29
 * @Name MIU
 */

public class MinDistance {

    public static void main ( String[] args ) {
        System.out.println(minDistance(13013));
        System.out.println(minDistance(8));
    }

    private static int minDistance ( int n ) {
        int min = Integer.MAX_VALUE;

        int currentFactor = 1;
        int nextFactor = getNextFactor(currentFactor,n);

        while ( n != nextFactor ) {
          if ( nextFactor - currentFactor < min ){
              min = nextFactor-currentFactor;
          }
          currentFactor = nextFactor;
          nextFactor = getNextFactor(currentFactor,n);
        }

        return min;
    }

    private static int getNextFactor ( int currentFactor, int n ) {
        for ( int i = currentFactor+1; i <= n; i++ ) {
            if ( n % i == 0 ){
                return i;
            }

        }
        return 0;
    }
}
