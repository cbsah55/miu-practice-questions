package com.cbs.phase2;

import java.util.Arrays;
import java.util.Collections;

/**
 * Write a function fill with signature  int[ ] fill(int[ ] arr, int k, int n)   which does the following:
 * It returns an integer array arr2 of length n whose first k elements are the  same as the first k elements of arr,
 * and whose remaining elements consist of repeating blocks of the  first k elements.
 * You can assume array arr has at least k elements. The function should return null if  either k or n is not positive.
 * Examples:
 * fill({1,2,3,5, 9, 12,-2,-1}, 3, 10) returns {1,2,3,1,2,3,1,2,3,1}.
 * fill({4, 2, -3, 12}, 1, 5)  returns {4, 4, 4, 4, 4}.
 * fill({2, 6, 9, 0, -3}, 0, 4) returns null.
 *
 * @Author
 * @Created 2024/03/28
 * @Name MIU
 */

public class FillArray {

    public static void main ( String[] args ) {
        int[] arr1 = {1,2,3,5, 9, 12,-2,-1};
        int[] arr2 = {4, 2, -3, 12};
        int[] arr3 = {2, 6, 9, 0, -3};

        System.out.println(Arrays.toString(fill(arr1,3,10)));
        System.out.println(Arrays.toString(fill(arr2,1,5)));
        System.out.println(Arrays.toString(fill(arr3,0,4)));
    }

    private static int[] fill ( int[] arr, int k, int n ) {
        int[] newArr = new int[n];
        int counter = 0;

        if ( k <= 0 || n <= 0 ){
            return null;
        }
            while ( counter < n ) {
                for ( int j = 0; j < k; j++ ) {
                    if ( counter < n ){
                        newArr[counter] = arr[j];
                        counter++;

                    }
                }
            }




        return newArr;
    }
}
