package com.cbs.phase2;

/**
 * A normal number is defined to be one that has no odd factors, except for 1 and possibly itself.
 * Write a method named isNormal that returns 1 if its integer argument is normal, otherwise it returns 0.
 * The  function signature is   int isNormal(int n)
 * Examples: 1, 2, 3, 4, 5, 7, 8 are normal numbers.
 * 6 and 9 are not normal numbers since 3 is an odd factor.
 * 10 is  not a normal number since 5 is an odd factor.
 *
 * @Author
 * @Created 2024/04/02
 * @Name MIU
 */

public class NormalNumber {

    public static void main ( String[] args ) {
        System.out.println(isNormal(4));
        System.out.println(isNormal(5));
        System.out.println(isNormal(7));
        System.out.println(isNormal(8));
        System.out.println(isNormal(6));
        System.out.println(isNormal(9));
        System.out.println(isNormal(10));
    }

    private static int isNormal ( int num ) {
        for ( int i = 2; i < num; i++ ) {
           if ( num % i ==0 && i % 2 !=0 ){
               return 0;
           }
        }
        return 1;
    }
}
