package com.cbs.phase2;

import java.util.Arrays;

/**
 * Write a function to return an array containing all elements common to two given arrays containing distinct positive integers.
 * You should not use any inbuilt methods. You are allowed to use any number of arrays.
 * The signature of the function is: int[] f(int[] first, int[] second)
 * Examples:
 * --------------------------------|--------------------------------------------------------------
 * | if the input parameters are    | return                                                       |
 * |--------------------------------|--------------------------------------------------------------|
 * | {1,8,3,2},{4,2,6,1}            | {1,2}                                                        |
 * |--------------------------------|--------------------------------------------------------------|
 * | {1,8,3,2,6},{2,6,1}            | {2,6,1}                                                      |
 * |--------------------------------|--------------------------------------------------------------|
 * | {1,3,7,9},{7,1,9,3}            | {1,3,7,9}                                                    |
 * |--------------------------------|--------------------------------------------------------------|
 * | {1,2}, {3,4}                   | {}                                                           |
 * |--------------------------------|--------------------------------------------------------------|
 * | {}, {1,2,3}                    | {}                                                           |
 * |--------------------------------|--------------------------------------------------------------|
 * | {1,2}, {}                      | {}                                                           |
 * |--------------------------------|--------------------------------------------------------------|
 * | {1,2}, null                    | null                                                         |
 * |--------------------------------|--------------------------------------------------------------|
 * | null, {}                       | null                                                         |
 * |--------------------------------|--------------------------------------------------------------|
 * | null, null                     | null                                                         |
 * --------------------------------|--------------------------------------------------------------
 *
 * @Author
 * @Created 2024/04/05
 * @Name MIU
 */

public class CommonArrayElements {

    public static void main ( String[] args ) {
        int[] arr = {1,8,3,2};
        int[] arrr = {4,2,6,1};
        int[] arr1 = {1,8,3,2,6};
        int[] arr11 = {2,6,1} ;
        int[] arr2 = {1,3,7,9};
        int[] arr22 = {7,1,9,3} ;
        int[] arr3 = {1,2};
        int[] arr33 = {3,4};
        int[] arr4 = {};
        int[] arr44 = {1,2,3};
        int[] arr5 = {1,2};
        int[] arr55 = {};
        int[] arr6 = {1,2};
        int[] arr66 = null;
        int[] arr7 = null;
        int[] arr77 = {};
        int[] arr8 = null;
        int[] arr88 = null;

        System.out.println(f(arr,arrr));
        System.out.println(f(arr1,arr11));
        System.out.println(f(arr2,arr22));
        System.out.println(f(arr3,arr33));
        System.out.println(f(arr4,arr44));
        System.out.println(f(arr5,arr55));
        System.out.println(f(arr6,arr66));
        System.out.println(f(arr7,arr77));
        System.out.println(f(arr8,arr88));
    }

    private static String f ( int[] firstArray, int[] secondArray ) {
        if ( firstArray == null || secondArray == null ){
            return null;
        }

        int commonCount= 0;
        for ( int i = 0; i < firstArray.length; i++ ) {
            for ( int j = 0; j < secondArray.length; j++ ) {
               if ( firstArray[i] == secondArray[j] ){
                   commonCount++;
               }
            }
        }

        int[] returnArray = new int[commonCount];
        int arrayCounter = 0;
        for ( int i = 0; i < firstArray.length; i++ ) {
            for ( int j = 0; j < secondArray.length; j++ ) {
                if ( firstArray[i] == secondArray[j] ){
                    returnArray[arrayCounter] = firstArray[i];
                    arrayCounter++;
                }
            }
        }

        return Arrays.toString(returnArray);
    }
}
