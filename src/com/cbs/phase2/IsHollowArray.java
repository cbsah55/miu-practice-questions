package com.cbs.phase2;

/**
 * An array is said to be hollow if it contains 3 or more zeroes in the middle that are preceded and
 * followed by the same number of non-zero elements. Write a function named isHollow that  accepts an
 * integer array and returns 1 if it is a hollow array, otherwise it returns 0
 * Examples:
 * isHollow({1,2,4,0,0,0,3,4,5}) returns 1.
 * isHollow ({1,2,0,0,0,3,4,5}) returns 0.
 * isHollow  ({1,2,4,9, 0,0,0,3,4, 5}) returns 0.
 * isHollow ({1,2, 0,0, 3,4}) returns 0.
 * If you are programming in Java or C#,
 * the function signature is  int isHollow(int[ ] a).
 * If you are C or C++ programmer  int isHollow(int[ ] a,  int len)
 * where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/03/28
 * @Name MIU
 */

public class IsHollowArray {

    public static void main ( String[] args ) {
        int[] arr1 = {1,2,4,0,0,0,3,4,5};
        int[] arr2 = {1,2,0,0,0,3,4,5};
        int[] arr3 = {1,2,4,9, 0,0,0,3,4, 5};
        int[] arr4 = {1,2, 0,0, 3,4};
        int[] arr5 = {1,2, 1,0,1, 3,4};
        int[] arr6 = {1,0,0,0, 2,0,0,4};

        int[] arr7 = {1,0,4,0,0,0,3,0,5};


        System.out.println(isHollow(arr1));
        System.out.println(isHollow(arr2));
        System.out.println(isHollow(arr3));
        System.out.println(isHollow(arr4));
        System.out.println(isHollow(arr5));
        System.out.println(isHollow(arr6));
        System.out.println(isHollow(arr7));

    }

    private static int isHollow ( int[] arr1 ) {

        int midPos = arr1.length /2;
        int zeroCount = 0;
        int leftIndex = midPos-1;
        int rightIndex = midPos +1;

        if ( arr1.length < 3 ){
            return 0;
        }
        //update the zero count
        if ( arr1[midPos] == 0 ){
            zeroCount = 1;
        }

        // loop until non zero element and update the nonzero index
        while ( arr1[leftIndex] ==0 && arr1[ rightIndex] == 0 ){
             leftIndex--;
             rightIndex++;
             zeroCount+=2;
//             if ( arr1[leftIndex] ==0 ){
//                 leftIndex--;
//                 zeroCount++;
//
//             }
//             if (arr1[rightIndex] ==0  ){
//                rightIndex++;
//                 zeroCount++;
//             }

        }

        //if non-zero not greater than 3 return false;
        if ( zeroCount < 3 ){
            return 0;
        }

        //iterate through each non zero index and update the count
        int leftCount =0, rightCount = 0;
        for ( int i = 0; i <= leftIndex; i++ ) {
           if ( arr1[i] !=0 ){
               leftCount++;
           }else {
               return 0;
           }
        }

        for ( int i = rightIndex; i < arr1.length; i++ ) {
            if ( arr1[i] !=0 ){
                rightCount++;
            }else {
                return 0;
            }
        }

        return leftCount == rightCount ? 1 : 0;
    }
}
