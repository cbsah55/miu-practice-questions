package com.cbs.phase2;

/**
 * Consider the prime number 11. Note that 13 is also a prime and 13 – 11 = 2. So, 11 and 13 are  known as twin primes.
 * Similarly, 29 and 31 are twin primes. So is 71 and 73. However, there are  many primes for which there is no twin.
 * Examples are 23, 67. A twin array is defined to an array  which every prime that has a twin appear with a twin.
 * Some examples are
 * {3, 5, 8, 10, 27},       // 3 and 5 are twins and both are present
 * {11, 9, 12, 13, 23}, // 11 and 13 are twins and both are present, 23 has no twin
 * {5, 3, 14, 7, 18, 67}.      // 3 and 5 are twins, 5 and 7 are twins, 67 has no twin
 * The following are NOT twin arrays:
 * {13, 14, 15, 3, 5}   // 13 has a twin prime and it is missing in the array
 * {1, 17, 8, 25, 67}     // 17 has a twin prime and it is missing in the array
 * Write a function named isTwin(int[ ] arr) that returns 1 if its array argument is a Twin array, otherwise  it returns 0.
 *
 * @Author
 * @Created 2024/03/29
 * @Name MIU
 */

public class IsTwinArray {

    public static void main ( String[] args ) {
        int[] arr1 = {3, 5, 8, 10, 27};
        int[] arr2 = {11, 9, 12, 13, 23};
        int[] arr3 = {5, 3, 14, 7, 18, 67};
        int[] arr4 = {13, 14, 15, 3, 5};
        int[] arr5 = {1, 17, 8, 25, 67};


        System.out.println(isTwinArray(arr1));
        System.out.println(isTwinArray(arr2));
        System.out.println(isTwinArray(arr3));
        System.out.println(isTwinArray(arr4));
        System.out.println(isTwinArray(arr5));

    }

    private static int isTwinArray ( int[] array ) {
        int isTwin = 0;
        for ( int i = 0; i < array.length; i++ ) {

            if ( isPrime(array[i]) ) {
                int currNum = array[i];
                int nextPrime = nextPrime(currNum + 1);
                int prevPrime = prevPrime(currNum - 1);
                boolean isTwinEligible = nextPrime - currNum == 2 || currNum - prevPrime == 2;
                if ( isTwinEligible ) {
                    for ( int j = 0; j < array.length; j++ ) {
                        if ( array[j] == prevPrime || array[j] == nextPrime ) {
                            isTwin = 1;
                            break;
                        } else {
                            isTwin = 0;
                        }

                        if ( j == array.length - 1 && isTwin == 0 ) {
                            return 0;
                        }

                    }
                }

            }
        }
        return isTwin;
    }


    private static boolean isPrime ( int n ) {
        if ( n <= 1 ) {
            return false;
        }

        for ( int i = 2; i < n; i++ ) {
            if ( n % i == 0 ) {
                return false;
            }
        }

        return true;
    }

    private static int prevPrime ( int n ) {
        while ( !isPrime(n) ) {
            n--;
        }
        return n;
    }

    private static int nextPrime ( int n ) {
        while ( !isPrime(n) ) {
            n++;
        }

        return n;
    }
}
