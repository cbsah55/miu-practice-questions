package com.cbs.phase2;

/**
 * . Mode is the most frequently appearing value.
 * Write a function  named hasSingleMode that takes an array argument and returns 1 if the mode value in its
 * array argument occurs exactly once in the array, otherwise it returns 0.
 * If you are writing in  Java or C#, the function signature is  int hasSingleMode(int[ ] ).
 * If you are writing in C or C++, the function signature is  int hasSingleMode(int a[ ], int len)  where len is the length of a.
 * Examples
 * Array elements               | Mode values           | Value returned      |  Comments
 *  1, -29, 8, 5, -29, 6        | -29                   |    1                   | single mode
 * 1, 2, 3, 4, 2, 4, 7          |2, 4                   |    0                   | no single mode
 * 1, 2, 3, 4, 2, 4, 7          |1, 2, 3, 4, 6          |    0                   | no single mode
 * 7, 1, 2, 1, 7, 4, 2, 7,      |   7                   |    1                    |   single mode
 *
 * @Author
 * @Created 2024/04/07
 * @Name MIU
 */

public class SingleMode {

    public static void main ( String[] args ) {
        int[] arr = {1, -29, 8, 5, -29, 6 };
        int[] arr1 = {1, 2, 3, 4, 2, 4, 7};
        int[] arr2 = {1, 2, 3, 4, 6};
        int[] arr3 = {7, 1, 2, 1, 7, 4, 2, 7};
        int[] arr4 = {7, 1, 2, 1, 7, 4, 2, 7,2};
        int[] arr5 = {7};
        int[] arr6 = {7,2};
        int[] arr7 = {7,7};

        System.out.println(hasSingleMode(arr));
        System.out.println(hasSingleMode(arr1));
        System.out.println(hasSingleMode(arr2));
        System.out.println(hasSingleMode(arr3));
        System.out.println(hasSingleMode(arr4));
        System.out.println(hasSingleMode(arr5));
        System.out.println(hasSingleMode(arr6));
        System.out.println(hasSingleMode(arr7));

    }

    private static int hasSingleMode ( int[] arr ) {
        int maxCount = Integer.MIN_VALUE;
         int freq = 1;

         if ( arr.length <1  ){
             return 0;
         }

         if ( arr.length ==1 ){
             return 1;
         }

        for ( int i = 0; i < arr.length; i++ ) {
            int currentCount = 0;
            for ( int j = i; j < arr.length; j++ ) {
               if ( arr[i] == arr[j] ){
                   currentCount++;
               }
            }

            if ( currentCount == maxCount ){
                freq++;
            }
            if ( currentCount > maxCount ){
                maxCount = currentCount;
            }

        }

        return maxCount > 1 && freq <= 1? 1 : 0;
    }

}
