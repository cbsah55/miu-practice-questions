package com.cbs.phase2;

/**
 * Define a square pair to be the tuple <x,y> where x and y are positive, non-zero integers, x<y and x+y is a perfect square.
 * A perfect square is an integer whose square root is also an integer, e.g 4,9,16 are perfect squares but 3,10,and 17 are not.
 * Write a function named countSquarePairs that takes an array and returns the number of square pairs that can be constructed from
 * the elements in the array.
 * For example, if  the array is {11,5,4,20} the function would return 3 because the only square pairs that can be constructed
 * from those numbers are <5,11>,<5,20> and <4,5>.
 * You may assume that there exists a function named isPerfectSquare that returns 1 if its argument is a perfect square and 0 otherwise.
 * isPerfectSquare(4) returns 1 and isPerfectSquare(8) returns 0.
 * If you are programming in Java or C#, the function signature is int countSquarePairs(int[] a) You may assume that there are no duplicate values in the array. ie you don't have to deal with an array like {2,7,2,2}.
 *
 * @Author
 * @Created 2024/04/04
 * @Name MIU
 */

public class SquarePair {

    public static void main ( String[] args ) {
        int[] arr1 = {11,5,4,20};
        int[] arr2 =  {9,0,2,-5,7} ;
        int[] arr3 = {9};


        System.out.println(countSquarePairs(arr1));
        System.out.println(countSquarePairs(arr2));
        System.out.println(countSquarePairs(arr3));
    }

    private static int countSquarePairs ( int[] array ) {
        int count = 0;

        for ( int i = 0; i < array.length; i++ ) {
            for ( int j = i+1; j < array.length; j++ ) {
               if ( array[i] > 0 && array[j]>0 && isPerfectSquare(array[i] + array[j]) ){
                   count++;
               }
            }
        }

        return count;
    }

    private static boolean isPerfectSquare(int n){
        for ( int i = 2; i < n; i++ ) {
             if ( n % i ==0 && n / i == i ){
                 return true;
             }
        }

        return false;
    }
}
