package com.cbs.phase2;

import static com.cbs.phase2.MagicArray.isPrime;

/**
 * A primeproduct is a positive integer that is the product of exactly two primes greater than 1.
 * For  example, 22 is primeproduct since 22 = 2 times 11 and both 2 and 11 are primes greater than 1.
 * Write a function named isPrimeProduct  with an integer parameter that returns 1 if the parameter is a  primeproduct,
 * otherwise it returns 0. Recall that a prime number is a positive integer with no factors  other than 1 and itself.
 * You may assume that there exists a function named isPrime(int m) that returns 1 if its m is a prime  number.
 * You do not need to write isPrime. You are allowed to use this function.
 * The function signature   int isPrimeProduct(int n)
 *
 * @Author
 * @Created 2024/03/20
 * @Name MIU
 */

public class PrimeProduct {

    public static void main ( String[] args ) {
        System.out.println(isPrimeProduct(22));
        System.out.println(isPrimeProduct(33));
        System.out.println(isPrimeProduct(35));
        System.out.println(isPrimeProduct(36));
    }

    private static int isPrimeProduct ( int n ) {
        int[] primeNumbers = new int[n/2];
        int counter= 0;
        int isPrimeProduct = 0;

        for ( int i = 2; i < n; i++ ) {

            if ( isPrime(i) ){
                for ( int j = i+1; j < n; j++ ) {
                    if ( isPrime(j) && i * j ==n  ){
                        isPrimeProduct =1;
                        break;
                    }
                }
            }
        }
         // below also works

//        for ( int i = 2; i < n; i++ ) {
//
//            if ( isPrime(i) ){
//                primeNumbers[counter]= i;
//                counter++;
//            }
//        }



//        for ( int i = 0; i < primeNumbers.length; i++ ) {
//            for ( int j = 0; j < primeNumbers.length; j++ ) {
//                if ( primeNumbers[i] * primeNumbers[j] == n ){
//                    isPrimeProduct =1;
//                    break;
//                }
//            }
//        }

        return isPrimeProduct;
    }


}
