package com.cbs.phase2;

/**
 * A non-empty array of length n is called an array of all possibilities, if it contains all numbers between 0 and n - 1  inclusive.
 * Write a method named isAllPossibilities that accepts an integer array and returns 1 if the array is an array of all
 * possibilities, otherwise it returns 0.
 * Examples {1, 2, 0, 3} is an array of all possibilities,
 * {3, 2, 1, 0} is an array of all possibilities,
 * {1, 2, 4,  3} is not an array of all possibilities, (because 0 not included and 4 is too big),
 * {0, 2, 3} is not an array of all possibilities, (because 1 is  not included),
 * {0} is an array of all possibilities, {} is not an array of all possibilities (because array is empty).
 * If you are programming in Java or C#, the function signature is    int isAllPossibilities(int[ ] a)
 * If you are programming in C or C++, the function signature is    int isAllPossibilities(int a[ ], int len)   where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/04/02
 * @Name MIU
 */

public class AllPossibilityArray {

    public static void main ( String[] args ) {
        int[] arr1 = {1, 2, 0, 3};
        int[] arr2 = {3, 2, 1, 0};
        int[] arr3 = {1, 2, 4,  3};
        int[] arr4 = {0, 2, 3};
        int[] arr5 = {0};

        System.out.println(isAllPossibilities(arr1));
        System.out.println(isAllPossibilities(arr2));
        System.out.println(isAllPossibilities(arr3));
        System.out.println(isAllPossibilities(arr4));
        System.out.println(isAllPossibilities(arr5));
    }

    private static int isAllPossibilities ( int[] array ) {
        int arrLen = array.length;
        boolean isPossible = false;

        for ( int i = 0; i < arrLen; i++ ) {
            for ( int j = 0; j <arrLen ; j++ ) {
                if ( array[j] == i ){
                    isPossible = true;
                    break;
                }  else {
                    isPossible = false;
                }
            }
            if ( !isPossible ){
                return 0;
            }
        }
        return 1;
    }
}
