package com.cbs.phase2;

/**
 * An array is defined to be odd-heavy if it contains at least one odd element and every odd element
 * is greater than every even element. So {11, 4, 9, 2, 8} is odd-heavy because the two odd elements (11 and 9)
 * are greater than all the even elements. And {11, 4, 9, 2, 3, 10} is not odd-heavy because the even element 10
 * is  greater than the odd element 9.  Write a function called isOddHeavy that accepts an integer array and returns  1
 * if the array is odd-heavy; otherwise it returns 0.
 * Some other examples: {1} is odd-heavy,
 * {2} is not odd heavy,
 * {1, 1, 1, 1} is odd-heavy,
 * {2, 4, 6, 8, 11} is odd-heavy,
 * {-2, -4, -6, -8, -11} is not odd-heavy.
 * If you are programming in Java or C#, the function signature is
 * int isOddHeavy(int[ ] a)
 * If you are programming in C or C++, the function signature is    int isOddHeavy(int a[ ], int len)   where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/04/02
 * @Name MIU
 */

public class OddHeavy {

    public static void main ( String[] args ) {
        int[] arr1 = {1};
        int[] arr2 = {2};
        int[] arr3 = {1, 1, 1, 1};
        int[] arr4 = {2, 4, 6, 8, 11};
        int[] arr5 = {11, 4, 9, 2, 8};
        int[] arr6 = {-2, -4, -6, -8, -11};
        int[] arr7 = {11, 4, 9, 2, 3, 10};

        System.out.println(isOddHeavy(arr1));
        System.out.println(isOddHeavy(arr2));
        System.out.println(isOddHeavy(arr3));
        System.out.println(isOddHeavy(arr4));
        System.out.println(isOddHeavy(arr5));
        System.out.println(isOddHeavy(arr6));
        System.out.println(isOddHeavy(arr7));
    }

    private static int isOddHeavy ( int[] arr1 ) {
        boolean hasOdd = false;

        for ( int i = 0; i < arr1.length; i++ ) {
            hasOdd = true;
            if ( arr1[i] %2 != 0 ) {
                for ( int j = 0; j < arr1.length; j++ ) {
                    if ( arr1[j] % 2 == 0 && arr1[i] < arr1[j] ){
                        return 0;
                    }
                }
            }
        }

        return hasOdd ? 1 : 0;
    }
}
