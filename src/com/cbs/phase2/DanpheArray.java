package com.cbs.phase2;

/**
 * A Daphne array is defined to be an array that contains at least one odd number and  begins and
 * ends with the same number of even numbers.
 * So {4, 8, 6, 3, 2, 9, 8,11, 8, 13, 12, 12, 6} is a Daphne array because it begins with three even  numbers
 * and ends with three even numbers and it contains at least one odd number
 * The array {2, 4, 6, 8, 6} is not a Daphne array because it does not contain an odd number.
 * The array {2, 8, 7, 10, -4, 6} is not a Daphne array because it begins with two even numbers but ends  with three even numbers.
 * Write a function named isDaphne that returns 1 if its array argument is a Daphne array. Otherwise, it  returns 0.
 * If you are writing in Java or C#, the function signature is     int isDaphne (int[ ] a)
 * If you are writing in C or C++, the function signature is     int isDaphne (int a[ ], int len) where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/04/02
 * @Name MIU
 */

public class DanpheArray {

    public static void main ( String[] args ) {

        int[] arr1 = {4, 8, 6, 3, 2, 9, 8,11, 8, 13, 12, 12, 6};
        int[] arr2 = {2, 4, 6, 8, 6};
        int[] arr3 = {2, 8, 7, 10, -4, 6};


        System.out.println(isDaphne(arr1));
        System.out.println(isDaphne(arr2));
        System.out.println(isDaphne(arr3));

    }

    private static int isDaphne ( int[] array ) {
        boolean hasOdd = false;

        for ( int i = 0,j=array.length-1; i < array.length; i++ ,j--) {
            if ( array[i] % 2 !=0 || array[j] % 2 !=0 ){
                hasOdd = true;
            }

            if ( array[i] % 2 ==0 && array[j] % 2 !=0 ) {
                return 0;
            }
        }

        return hasOdd ? 1 : 0;
    }


}
