package com.cbs.phase2;

/**
 * An array is said to be dual if it has an even number of elements and each pair of consecutive even and odd elements
 * sum to the same value. Write a function named isDual that accepts an array of integers and returns 1 if the array is dual,
 * otherwise it  returns 0.
 * Examples:  {1, 2, 3, 0} is a dual array (because 1+2 = 3+0 = 3),
 * {1, 2, 2, 1, 3, 0} is a dual array (because 1+2 = 2+1 = 3+0 = 3),
 * {1, 1, 2, 2}</td> is not a dual array (because 1+1 is not equal to 2+2),
 * {1, 2, 1}</td>    <td> is not a dual array (because array does not  have an even number of elements),
 * {} is a dual array.
 * If you are programming in Java or C#, the function signature is
 * int isDual(int[ ] a)
 * If you are programming in C or C++, the function signature is    int isDual(int a[ ], int len)   where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/04/02
 * @Name MIU
 */

public class DualArray {

    public static void main ( String[] args ) {
        int[] arr1 = {1, 2, 3, 0};
        int[] arr2 = {1, 2, 2, 1, 3, 0};
        int[] arr3 = {1, 1, 2, 2};
        int[] arr4 = {1, 2, 1};
        int[] arr5 = {};

        System.out.println(isDual(arr1));
        System.out.println(isDual(arr2));
        System.out.println(isDual(arr3));
        System.out.println(isDual(arr4));
        System.out.println(isDual(arr5));
    }

    private static int isDual ( int[] arr1 ) {
        if ( arr1.length % 2 !=0 ){
            return 0;
        }

        if ( arr1.length > 2 ) {
            int sum = arr1[0] + arr1[1];
            for ( int i = 0; i < arr1.length; i += 2 ) {
                int tempSum = arr1[i] + arr1[i + 1];
                if ( tempSum != sum ) {
                    return 0;
                }

            }
        }

        return 1;
    }
}
