package com.cbs.phase2;

/**
 * @Author
 * @Created 2024/04/05
 * @Name MIU
 */

public class NextPerfectSquare {

    public static void main ( String[] args ) {
        System.out.println(nextPerfectSquare(6));
        System.out.println(nextPerfectSquare(36));
        System.out.println(nextPerfectSquare(0));
        System.out.println(nextPerfectSquare(-5));
    }

    private static int nextPerfectSquare ( int n ) {

        if ( n < 0 ){
            return 0;
        }

        while ( true ){
            n++;
           if ( isPerfectSquare(n) ){
               return n;
           }
        }
    }


    private static boolean isPerfectSquare(int n){
        if ( n == 1 ){
            return true;
        }
        for ( int i = 2; i < n; i++ ) {
           if ( n % i ==0 && n / i == i ) {
               return true;
           }
        }

        return false;
    }

}
