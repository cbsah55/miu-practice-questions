package com.cbs.phase2;

/**
 * @Author
 * @Created 2024/04/06
 * @Name MIU
 */

public class PrimeProduct2 {

    public static void main ( String[] args ) {
        System.out.println(isPrimeProduct(22));
        System.out.println(isPrimeProduct(33));
        System.out.println(isPrimeProduct(21));
        System.out.println(isPrimeProduct(9));
    }

    private static int isPrimeProduct ( int n ) {

        for ( int i = 2; i < n; i++ ) {
            if ( isPrime(i) )  {
                for ( int j = i; j < n; j++ ) {
                    if ( isPrime(j) && i * j == n ){
                        return 1;
                    }
                }
            }

        }

        return 0;
    }

    private static boolean isPrime ( int n){
        if ( n<= 1 )
            return false;


        for ( int i = 2; i < n; i++ ) {
           if ( n % i ==0 ){
               return false;
           }
        }

        return true;
    }

}
