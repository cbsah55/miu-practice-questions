package com.cbs.phase2;

/**
 * An array with an odd number of elements is said to be centered if all elements
 * (except the middle  one) are strictly greater than the value of the middle element.
 * Note that only arrays with an odd  number of elements have a middle element.
 * Write a function named isCentered that accepts an  integer array and returns 1 if it is a centered array,
 * otherwise it returns 0.
 * Examples: {5, 3, 3, 4, 5} is not a centered array (the middle element 3 is not strictly less than all other  elements),
 * {3, 2, 1, 4, 5} is centered (the middle element 1 is strictly less than all other elements),
 * {3,  2, 1, 4, 1} is not centered (the middle element 1 is not strictly less than all other elements),
 * {3, 2, 1, 1,  4, 6} is not centered (no middle element since array has even number of elements),
 * {} is not centered  (no middle element),
 * {1} is centered (satisfies the condition vacuously).
 * If you are programming in Java or C#, the function signature is    int isCentered(int[ ]
 * a)  If you are programming in C or C++, the function signature is    int isCentered(int a[ ], int len)
 * where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/03/21
 * @Name MIU
 */

public class CenteredArray {

    public static void main ( String[] args ) {
        int[] arr1 = {5, 3, 3, 4, 5};
        int[] arr2 = {3, 2, 1, 4, 5};
        int[] arr3 = {3,  2, 1, 4, 1};
        int[] arr4 = {3, 2, 1, 1,  4, 6};
        int[] arr5 = {};
        int[] arr6 = {1};

        System.out.println(isCentered(arr1));
        System.out.println(isCentered(arr2));
        System.out.println(isCentered(arr3));
        System.out.println(isCentered(arr4));
        System.out.println(isCentered(arr5));
        System.out.println(isCentered(arr6));
    }

    private static int isCentered ( int[] array ) {
        int arrayLength = array.length;

        if ( isEven(arrayLength) || arrayLength ==0 ){
            return 0;
        }

        int midPoint = arrayLength /2;

        int centerElement = array[midPoint];

        for ( int i = 0; i < midPoint; i++ ) {
             if ( array[i] <= centerElement  ) {
               return 0;
             }
        }

        for ( int i = midPoint+1; i < arrayLength; i++ ) {
            if ( array[i] <= centerElement  ) {
                return 0;
            }
        }

        return 1;
    }


    private static boolean isEven(int n){
        return n %2 ==0;
    }

}
