package com.cbs.phase2;

/**
 * Write a function that will return 1 if an integer array satisfies the following conditions and returns 0 otherwise.
 * 1. it has even numbers of elements
 * 2. Sum of all the numbers in the first half of the array is equal to the sum of all the numbers in the second half of the array.
 * If you are programming in Java, the function Signature is int answerThree(int[] a)
 * Examples  -------------------|--------|----------------------------------------------------------------------- |
 * a                 | return | Explanation|
 * |-------------------|--------|-----------------------------------------------------------------------| |
 * {5,4,3,2,3,4,6,1} | 1      | *There are 8 (even) number of elements in the array. Thus condition 1 satisfied.
 * | |                   |        | *The sum of all the numbers in the first half = 5+4+3+2 = 14
 * |  -------------------|--------|-----------------------------------------------------------------------
 *
 * @Author
 * @Created 2024/04/04
 * @Name MIU
 */

public class FirstSecondHalfSum {

    public static void main ( String[] args ) {
        int[] arr = {5,4,3,2,3,4,6,1};

        System.out.println(isFirstHalfSecondHalfSumEqual(arr));
    }

    private static int isFirstHalfSecondHalfSumEqual ( int[] arr ) {
        boolean conditionOne = false;
        if ( arr.length %2 ==0 ){
            conditionOne = true;
        }

        int midPos = arr.length /2, firstHalfSum = 0, secondHalfSum = 0;

        //calculate firstHalfSum
        for ( int i = 0; i < midPos; i++ ) {
            firstHalfSum += arr[i];
        }

        //calculate Sefond half sum
        for ( int i = midPos; i < arr.length; i++ ) {
             secondHalfSum += arr[i];
        }


        return conditionOne && firstHalfSum == secondHalfSum ? 1 : 0;
    }
}
