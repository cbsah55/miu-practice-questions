package com.cbs.phase2;

/**
 * A wave array is defined to an array which does not contain two even numbers or two odd  numbers in adjacent locations.
 * So {7, 2, 9, 10, 5}, {4, 11, 12, 1, 6}, {1, 0, 5} and {2} are all wave  arrays.
 * But {2, 6, 3, 4} is not a wave array because the even numbers 2 and 6 are adjacent to each  other.
 * Write a function named isWave that returns 1 if its array argument is a Wave array, otherwise it  returns 0.
 * If you are programming in Java or C#, the function signature is  int isWave (int [ ] a)
 * If you are programming in C or C++, the function signature is  int isWave (int a[ ], int len)
 * where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/03/29
 * @Name MIU
 */

public class WaveArray {

    public static void main ( String[] args ) {
        int[] arr1 = {7, 2, 9, 10, 5};
        int[] arr2 = {4, 11, 12, 1, 6};
        int[] arr3 = {1, 0, 5};
        int[] arr4 = {2};
        int[] arr5 = {2, 6, 3, 4};

        System.out.println(isWave(arr1));
        System.out.println(isWave(arr2));
        System.out.println(isWave(arr3));
        System.out.println(isWave(arr4));
        System.out.println(isWave(arr5));
    }

    private static int isWave ( int[] array ) {
        for ( int i = 0; i < array.length-1; i++ ) {
            if ( (array[i] % 2 ==0 && array[i+1] % 2 ==0) ) {
                return 0;
            }
        }

        return 1;
    }
}
