package com.cbs.phase2;

/**
 * Consider the positive integer 50. Note that 50 = 25 + 25 = 5^2 + 5^2 and 50 = 1 + 49 = 1^2 + 7^2,
 * Thus 50 can be expressed as a sum of the two squares in two different ways.
 * Write a method whether or not a positive integer n can be expressed as a sum of two squares in exactly two different ways.
 * The signature of the function is int answerOne(int n)
 *
 * @Author
 * @Created 2024/04/04
 * @Name MIU
 */

public class AnswerOne {

    public static void main ( String[] args ) {
        System.out.println(answerOne(50));
    }

    private static int answerOne ( int n ) {
        int count =0;
        for ( int i = 1; i < n; i++ ) {
            for ( int j = i; j < n; j++ ) {
                int sum = 0;
                sum = getSquare(i) + getSquare(j);
                if ( n == sum ){
                    count++;
                }
            }


        }

        return count == 2 ? 1: 0;
    }


    private static int getSquare(int n){
        return n * n;
    }
}
