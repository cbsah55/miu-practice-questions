package com.cbs.phase2;

/**
 * Define a Triple array to be an array where every value occurs exactly three times.
 * For example, {3, 1, 2, 1, 3, 1, 3, 2, 2} is a Triple array.
 * The following arrays are not Triple arrays  {2, 5, 2, 5, 5, 2, 5} (5 occurs four times instead of three times)
 * {3, 1, 1, 1} (3 occurs once instead of three times)
 * Write a function named isTriple that returns 1 if its array argument is a Triple array. Otherwise it returns 0.
 * If you are programming in Java or C#, the function signature is     int isTriple (int[ ] a)
 * If you are programming in C or C++, the function signature is     int isTriple (int a[ ], int len) where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/04/01
 * @Name MIU
 */

public class TripleArray {

    public static void main ( String[] args ) {
        int[] arr1 = {3, 1, 2, 1, 3, 1, 3, 2, 2};
        int[] arr2 = {2, 5, 2, 5, 5, 2, 5};
        int[] arr3 = {3, 1, 1, 1};

        System.out.println(isTriple(arr1));
        System.out.println(isTriple(arr2));
        System.out.println(isTriple(arr3));
    }

    private static int isTriple ( int[] array ) {
        for ( int i = 0; i < array.length; i++ ) {
            int currentCount = 0;
            for ( int j = 0; j < array.length; j++ ) {
                if ( array[i] == array[j] ){
                    currentCount++;
                }
            }
            if ( currentCount != 3 ){
                return 0;
            }
        }

        return 1;
    }
}
