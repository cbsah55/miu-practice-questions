package com.cbs.phase2;

/**
 * Write a function to reverse an integer using numeric operators and without using any arrays or other data structures.
 * The signature of the function is: int f(int n)
 * Examples
 * --------------------------------|--------------------------------------------------------------
 * | if the input integer is        | return                                                       |
 * |--------------------------------|--------------------------------------------------------------|
 * | 1234                           | 4321                                                         |
 * |--------------------------------|--------------------------------------------------------------|
 * | 12005                          | 50021                                                        |
 * |--------------------------------|--------------------------------------------------------------|
 * | 1                              | 1                                                            |
 * |--------------------------------|--------------------------------------------------------------|
 * | 1000                           | 1                                                            |
 * |--------------------------------|--------------------------------------------------------------|
 * | 0                              | 0                                                            |
 * |--------------------------------|--------------------------------------------------------------|
 * | -12345                         | -54321                                                       |
 * --------------------------------|--------------------------------------------------------------
 *
 * @Author
 * @Created 2024/04/04
 * @Name MIU
 */

public class ReverseInteger {

    public static void main ( String[] args ) {
        System.out.println(reverseInteger(1234));
        System.out.println(reverseInteger(12005));
        System.out.println(reverseInteger(1));
        System.out.println(reverseInteger(1000));
        System.out.println(reverseInteger(0));
        System.out.println(reverseInteger(-12345));
    }

    private static int reverseInteger ( int i ) {
        int reverse = 0;
        int sign = 1;

        if ( i < 0 ){
          sign = -1;
          i *= -1;
        }

//        while ( i > 0 ) {
//            int currentPlace = countPlace(i);
//            int lastDigit = i % 10;
//            reverse += lastDigit * currentPlace;
//            i = i /10;
//
//        }

        while (i != 0) {
            reverse = (reverse * 10) + (i % 10);
            i /= 10;
        }

       return sign * reverse;
    }

    private static int countPlace(int n){
        int place = 0;


        while ( n > 0 ){
            n = n /10;
            place++;
        }
        int returnPlace= 1;
        for ( int i = 1; i < place; i++ ) {
             returnPlace *= 10;
        }
        return returnPlace;
    }
}
