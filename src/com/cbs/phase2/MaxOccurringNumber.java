package com.cbs.phase2;

/**
 * Write a function that will return the most occurring number in an array.
 * If there is more than one such number, the function may return any one of them.
 * If you are programming in Java or C#, the function signature is int answerTwo(int[] a)
 * Examples  -------------------------|----------|------------------------------------------------------------ |
 * a                       | return   | Explanation
 * | |-------------------------|----------|------------------------------------------------------------|
 * | {6,8,1,8,2}             | 8        | 8 occurs two times. No other number occurs 3 or more times |
 * |-------------------------|----------|------------------------------------------------------------|
 * | {6,8,1,8,6}             | 8 or 6   | 8, 6. The Function may return either 8 or 6
 *
 * @Author
 * @Created 2024/04/04
 * @Name MIU
 */

public class MaxOccurringNumber {

    public static void main ( String[] args ) {
        int[] arr1 = {6,8,1,8,2};
        int[] arr2 = {6,8,1,8,6};
        int[] arr3 = {6,8,1,8,6,6};
        int[] arr5 = {1,2,3,4,5};
        int[] arr6 = {0,0,0,0,0,0,1};


        System.out.println(maxOccurringNumber(arr1));
        System.out.println(maxOccurringNumber(arr2));
        System.out.println(maxOccurringNumber(arr3));
        System.out.println(maxOccurringNumber(arr5));
        System.out.println(maxOccurringNumber(arr6));
    }

    private static int maxOccurringNumber ( int[] array ) {

        int prevNum = array[0];
        int prevCount = 1;
        for ( int i = 0; i < array.length; i++ ) {
            int currentNum = array[i];
            int currentCount = 1;
            for ( int j = i+1; j < array.length-1; j++ ) {
                if ( array[i] == array[j] ){
                   currentCount++;
                }
            }

            if ( currentCount > prevCount ){
                prevCount = currentCount;
                prevNum = currentNum;
            }
        }

        return prevNum;
    }
}
