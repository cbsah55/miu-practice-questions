package com.cbs;

/**
 * A Fibonacci number is a number in the sequence 1, 1, 2, 3, 5, 8, 13, 21,….
 * Note that first two Fibonacci numbers are  1 and any Fibonacci number other than the first two is the sum of the previous two Fibonacci numbers.
 * For example, 2 = 1  + 1, 3 = 2 + 1, 5 = 3 + 2 and so on.
 * Write a function named isFibonacci that returns 1 if its integer argument is a Fibonacci number, otherwise it returns 0.
 * The signature of the function is     int isFibonacci (int n)
 *
 * @Author
 * @Created 2024/01/24
 * @Name MIU
 */

public class FibonacciNumber {

    public static void main ( String[] args ) {
        System.out.println(isFibonacci(13));
        System.out.println(isFibonacci(15));
    }

    private static int isFibonacci ( int n ) {
        if ( n <=0 ){
           return 0;
        }

        int a = 0;
        int b = 1;

        while(b <n){
            int temp = b;
            b +=a;
            a = temp;
        }

        return  b == n ? 1 : 0;
    }

}
