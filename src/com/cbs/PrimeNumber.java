package com.cbs;

/**
 * @Author
 * @Created 2024/01/04
 * @Name MIU
 */

public class PrimeNumber {

    public static void main ( String[] args ) {
        System.out.println(primeCount(10,30));
        System.out.println(primeCount(11,29));
        System.out.println(primeCount(20,22));
        System.out.println(primeCount(1,1));
        System.out.println(primeCount(5,5));
        System.out.println(primeCount(6,2));
        System.out.println(primeCount(-10,6));
    }

    private static int primeCount ( int start, int end ) {
        int primeFreq = 0;
        if ( start > end ){
            return 0;
        }
        for ( int i = start; i <= end; i++ ) {
           if ( isPrime(i) ) {
               primeFreq++;
           }
        }
        return primeFreq;
    }

    private static boolean isPrime ( int i ) {
        if ( i <=1 ){
            return false;
        }

        for ( int j = 2; j < (int) Math.sqrt(i); j++ ) {
            if ( i % j == 0 ){
                return false;
            }
        }

        return true;

    }


}
