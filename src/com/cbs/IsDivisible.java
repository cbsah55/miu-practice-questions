package com.cbs;

/**
 * Write a method named isDivisible that takes an integer array and a divisor and returns 1
 * if all its elements are divided by the divisor with no remainder. Otherwise it returns 0.
 * If you are programming in Java or C#, the function signature is
 * int isDivisible(int [  ] a, int divisor)
 * If you are programming in C or C++, the function signature is
 * int isDivisible(int a[ ], int len, int divisor) where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/01/16
 * @Name MIU
 */

public class IsDivisible {

    public static void main ( String[] args ) {
        int[] arr = {3, 3, 6, 36};
        int[] arr1 = {4};
        int[] arr2 = {3, 4, 3, 6, 36};
        int[] arr3 = {6, 12, 24, 36};
        int[] arr4 = {};

        System.out.println(isDivisible(arr,3));
        System.out.println(isDivisible(arr1,2));
        System.out.println(isDivisible(arr2,3));
        System.out.println(isDivisible(arr3,12));
        System.out.println(isDivisible(arr4,5));
    }

    private static int isDivisible ( int[] arr, int divisor ) {
        for ( int i = 0; i < arr.length; i++ ) {
            if ( arr[i] % divisor != 0 ){
                return 0;
            }
        }

        return 1;
    }

}
