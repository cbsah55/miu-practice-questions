package com.cbs;

/**
 * A wave array is defined to an array which does not contain two even numbers or two odd  numbers in adjacent locations.
 * So {7, 2, 9, 10, 5}, {4, 11, 12, 1, 6}, {1, 0, 5} and {2} are all wave  arrays.
 * But {2, 6, 3, 4} is not a wave array because the even numbers 2 and 6 are adjacent to each  other.
 * Write a function named isWave that returns 1 if its array argument is a Wave array, otherwise it  returns 0.
 * If you are programming in Java or C#, the function signature is  int isWave (int [ ] a)
 * If you are programming in C or C++, the function signature is  int isWave (int a[ ], int len) where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/01/24
 * @Name MIU
 */

public class WaveArray {

    public static void main ( String[] args ) {
        int[] arr = {7, 2, 9, 10, 5};
        int[] arr1 = {4, 11, 12, 1, 6};
        int[] arr2 = {1, 0, 5};
        int[] arr3 = {2};
        int[] arr4 = {2, 6, 3, 4};

        System.out.println(isWave(arr));
        System.out.println(isWave(arr1));
        System.out.println(isWave(arr2));
        System.out.println(isWave(arr3));
        System.out.println(isWave(arr4));

    }

    private static int isWave ( int[] arr ) {
         int isWave = 1;
        for ( int i = 0; i < arr.length-1; i++ ) {
            if ( isEven(arr[i]) && isEven(arr[i+1]) ){
                isWave =  0;
                break;
            }
        }

        return isWave;
    }

    private static boolean isEven ( int i ) {
        return i % 2 ==0;
    }
}
