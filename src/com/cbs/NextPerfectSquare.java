package com.cbs;

import java.util.ArrayList;
import java.util.List;

/**
 * Write a function named nextPerfectSquare
 * <p>
 * that returns the first perfect square that is greater than its integer argument.
 * A perfect square is an integer that is equal to some integer squared.
 * For example 16 is a perfect square because 16 = 4* 4.
 * However 15 is not a perfect square because there is no integer n such that 15 = n*n.
 * <p>
 * Signature:
 * <p>
 * Python
 * int isPerfectSquare(int n)
 * <p>
 * Examples:
 * <p>
 * n	next perfect square
 * 6	9
 * 36	49
 * 0	1
 * -5	0
 */

public class NextPerfectSquare {

    public static void main ( String[] args ) {
        List<Integer> list = new ArrayList<>();
        list.add(6);
        list.add(36);
        list.add(0);
        list.add(-5);
        list.stream().map(NextPerfectSquare::nextPerfectSquare).forEach(System.out::println);
        list.stream().map(NextPerfectSquare::nextPerfectSquareV2).forEach(System.out::println);

    }

    private static int nextPerfectSquare ( int n ) {
        if ( n < 0 ){
            return 0;
        }
        double sqrt = Math.sqrt(n);
        return (int) Math.pow((int) sqrt + 1, 2);

    }

    private static int nextPerfectSquareV2(int n){
        int nextPerfectSquare = n +1;
        while(!isPerfectSquare(nextPerfectSquare)){
             nextPerfectSquare++;
        }
        return nextPerfectSquare;
    }

    private static boolean isPerfectSquare ( int nextPerfectSquare ) {
        int n = (int) Math.sqrt(nextPerfectSquare);

        return n * n == nextPerfectSquare;
    }

}
