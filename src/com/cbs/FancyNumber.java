package com.cbs;

/**
 * A fancy number is a number in the sequence 1, 1, 5, 17, 61, … .
 * Note that first two fancy numbers are 1 and any fancy  number other than the first two is sum of the three times previous one
 * and two times the one before that.
 * See below:
 * 1,
 * 1,
 * 3*1 +2*1 = 5
 * 3*5 +2*1 = 17
 * 3*17 + 2*5 = 61
 * Write a function named isFancy that returns 1 if its integer argument is a Fancy number, otherwise it returns 0.
 * The signature of the function is  int isFancy(int n)
 *
 * @Author
 * @Created 2024/01/24
 * @Name MIU
 */

public class FancyNumber {

    public static void main ( String[] args ) {
        System.out.println(isFancy(17));
        System.out.println(isFancy(61));
        System.out.println(isFancy(15));
    }

    private static int isFancy ( int n ) {
        if ( n <= 0 ) {
            return 0;
        }

        if ( n == 1 ) {
            return 1;
        }

        int a = 1;
        int b = 1;

        while ( b < n ) {
            int temp = b;
            b = a * 2 + b * 3;
            a = temp;

        }

        return (b == n) ? 1 : 0;
    }

}
