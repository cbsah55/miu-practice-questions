package com.cbs;

import java.util.Arrays;

/**
 * Write a function named minDistance that returns the smallest distance between two factors of a  number.
 * For example, consider 13013 = 1*7*11*13. Its factors are 1, 7, 11, 13 and 13013.
 * minDistance(13013) would return 2 because the smallest distance between any two factors is 2 (13 -  11 = 2).
 * As another example, minDistance (8) would return 1 because the factors of 8 are 1, 2, 4, 8
 * and the smallest distance between any two factors is 1 (2 – 1 = 1).
 * The function signature is  int minDistance(int n)
 *
 * @Author
 * @Created 2024/01/24
 * @Name MIU
 */

public class MinDistance {

    public static void main ( String[] args ) {
        System.out.println(minDistance(8));
        System.out.println(minDistance(13013));
    }

    private static int minDistance ( int n ) {
        int[] arr = new int[n];
        int arrIndex = 0;
        int minDistance = Integer.MAX_VALUE;

        for ( int i = 1; i <= n; i++ ) {
            if ( n % i == 0 ) {
                arr[arrIndex] = i;
                arrIndex++;
            }
        }

        for ( int i = 0; i < arrIndex; i++ ) {
            for ( int j = 0; j < arrIndex; j++ ) {
                if ( arr[i] > arr[j] ) {
                    int diff = arr[i] - arr[j];
                    if ( diff < minDistance )
                        minDistance = diff;
                }

            }
        }

        return minDistance;
    }

}
