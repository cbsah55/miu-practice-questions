package com.cbs;

/**
 * An array is said to be hollow if it contains 3 or more zeros in the middle that are preceded and
 * followed by the same number of non-zero elements.
 * Write a function named isHollow that accepts an integer  array and returns 1 if it is a hollow array, otherwise it returns 0.
 * The function signature is  int isHollow(int[ ] a).
 * Examples:  isHollow({1,2,4,0,0,0,3,4,5}) returns true.
 * isHollow ({1,2,0,0,0,3,4,5}) returns false. :
 * isHollow  ({1,2,4,9, 0,0,0,3,4, 5}) returns false.
 * isHollow ({1,2, 0,0, 3,4}) returns false.
 *
 * @Author
 * @Created 2024/01/24
 * @Name MIU
 */

public class HollowArray {

    public static void main ( String[] args ) {
        int[] arr = {1, 2, 4, 0, 0, 0, 3, 4, 5};
        int[] arr1 = {1, 2, 0, 0, 0, 3, 4, 5};
        int[] arr2 = {1, 2, 4, 9, 0, 0, 0, 3, 4, 5};
        int[] arr3 = {1, 2, 0, 0, 3, 4};


        System.out.println(isHollow(arr));
        System.out.println(isHollow(arr1));
        System.out.println(isHollow(arr2));
        System.out.println(isHollow(arr3));
    }

    private static int isHollow ( int[] arr ) {
        int middle = arr.length / 2;
        int startIndex = middle;
        int endIndex = middle;

        if ( arr[middle] != 0 ) {
            return 0;
        }

        while ( endIndex < arr.length ) {
            startIndex -= 1;
            endIndex += 1;

            if ( arr[startIndex] != 0 && arr[endIndex] != 0 ) {
                break;
            }
        }

        if ( startIndex + 1 == arr.length - endIndex ) {
            return 1;
        }

        return 0;

    }

}
