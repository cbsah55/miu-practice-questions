package com.cbs;

/**
 * The sum factor of an array is defined to be the number of times that the sum of the array appears as an element of the array.
 * So the sum factor of {1, -1, 1, -1, 1, -1, 1} is 4 because the sum of the elements of the array is 1 and 1 appears four times in the array.
 * And the sum factor of  {1, 2, 3, 4} is 0 because the sum of the elements of the array is 10 and 10 does not occur as an element of the array.
 * The sum factor of the empty array { } is defined to be 0.
 * Write a function named sumFactor that returns the sum factor of its array argument.
 * If you are programming in Java or C#, the function signature is
 * int sumFactor(int[ ] a)
 * If you are programming in C++ or C, the function signature is
 * int sumFactor(int a[ ], int len)  where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/01/11
 * @Name MIU
 */

public class SumFactor {

    public static void main ( String[] args ) {
        int[] arr = {3, 0, 2, -5, 0};
        int[] arr1 = {9, -3, -3, -1, -1};
        int[] arr2 = {1};
        int[] arr3 = {0, 0, 0};
        int[] arr4 = {};

        System.out.println(sumFactor(arr));
        System.out.println(sumFactor(arr1));
        System.out.println(sumFactor(arr2));
        System.out.println(sumFactor(arr3));
        System.out.println(sumFactor(arr4));
    }

    private static int sumFactor ( int[] arr ) {
        int sumOfArray = 0;
        int sumFactor = 0;

        for ( int i = 0; i < arr.length; i++ ) {
           sumOfArray += arr[i];
        }

        for ( int i = 0; i < arr.length; i++ ) {
            if ( arr[i] == sumOfArray ) sumFactor++;
        }

        return sumFactor;
    }

}
