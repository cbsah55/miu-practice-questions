package com.cbs;

/**
 * An array is called centered-15 if some consecutive sequence of elements of the array sum to 15 and this sequence is
 * preceded and followed by the same number of elements.
 * For example  {3, 2, 10, 4, 1, 6, 9} is centered-15 because  the sequence 10, 4, 1 sums to 15 and the sequence
 * is preceded by two elements (3, 2) and followed by two elements(6,9).
 * Write a method called isCentered15 that returns 1 if its array argument is centered-15, otherwise it returns 0.
 * If you are programming in Java or C#, the function prototype is
 * int isCentered15(int[ ] a)
 * If you are programming in C++ or C, the function prototype is
 * int isCentered5(int a[ ], int len) where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/01/11
 * @Name MIU
 */

public class CenteredFifteen {

    public static void main ( String[] args ) {
        int[] arr = {3, 2, 10, 4, 1, 6, 9};
        int[] arr1 = {2, 10, 4, 1, 6, 9};
        int[] arr2 = {3, 2, 10, 4, 1, 6};
        int[] arr3 = {1, 1, 8, 3, 1, 1};
        int[] arr4 = {9, 15, 6};
        int[] arr5 = {1, 1, 2, 2, 1, 1};
        int[] arr6 = {1, 1, 15 - 1, -1};


        System.out.println(isCentered15(arr));
        System.out.println(isCentered15(arr1));
        System.out.println(isCentered15(arr2));
        System.out.println(isCentered15(arr3));
        System.out.println(isCentered15(arr4));
        System.out.println(isCentered15(arr5));
        System.out.println(isCentered15(arr6));

        System.out.println("####################");

        System.out.println(isCentered15V2(arr));
        System.out.println(isCentered15V2(arr1));
        System.out.println(isCentered15V2(arr2));
        System.out.println(isCentered15V2(arr3));
        System.out.println(isCentered15V2(arr4));
        System.out.println(isCentered15V2(arr5));
        System.out.println(isCentered15V2(arr6));
    }

    private static int isCentered15 ( int[] arr ) {
        int length = arr.length;
        for ( int i = 0; i < length; i++ ) {
            int sum = 0;

            for ( int j = i; j < length; j++ ) {
                sum += arr[j];
                if ( sum == 15 && i == length - 1 - j ) {   // length -1 -j calculates the length of remaining index of array after index that sums to 15
                    // for eg  {2, 10, 4, 1, 6, 9} here 10 + 4 + 1 = 15, and length-1-j = 2
                    return 1;
                }
            }
        }

        return 0;
    }

    private static int isCentered15V2 ( int[] arr ) {
        int isCentered = 0;
        int lowerIndex = 0;
        int upperIndex = 0;
        int sum = 0;

        int length = arr.length;

        if ( length % 2 == 0 ) {
            lowerIndex = (length / 2) - 1;
            upperIndex = length / 2;
        } else {
            lowerIndex = (length - 1) / 2;
            upperIndex = lowerIndex;
        }

        while ( lowerIndex >= 0 && upperIndex < length ) {
            sum = 0;
            for ( int i = lowerIndex; i <= upperIndex; i++ ) {
                sum += arr[i];
            }
            if ( sum == 15 ) {
                isCentered = 1;
                break;
            }
            lowerIndex--;
            upperIndex++;
        }
        return isCentered;
    }

}
