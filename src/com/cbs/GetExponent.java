package com.cbs;

/**
 * . Write a method named getExponent(n, p) that returns the largest exponent x such that p^x evenly divides n.
 * If p is <= 1 the method  should return -1.
 * For example, getExponent(162, 3) returns 4 because 162 = 2^1 * 3^4, therefore the value of x here is 4.
 * The method signature is  int getExponent(int n, int p)
 * Examples:
 * if n is      and p        is return                              because
 * 27              3            3                       3<sup>3</sup> divides 27 evenly but 3<sup>4</sup> does not.
 * 28              3            0                       3<sup>0</sup> divides 28 evenly but 3<sup>1</sup> does not.
 * 280             7            1                       7<sup>1</sup> divides 280 evenly but 7<sup>2</sup> does not.
 * -250            5            3                       5<sup>3</sup> divides -250 evenly but 5<sup>4</sup> does not.
 * 18              1           -1                       if p <=1 the function returns -1.
 * 128             4            3                       4<sup>3</sup> divides 128 evenly but 4<sup>4</sup> does not.
 *
 * @Author
 * @Created 2024/01/25
 * @Name MIU
 */

public class GetExponent {

    public static void main ( String[] args ) {

        System.out.println(getExponent(162,3));
        System.out.println(getExponent(27,3));
        System.out.println(getExponent(28,3));
        System.out.println(getExponent(280,7));
        System.out.println(getExponent(-250,5));
        System.out.println(getExponent(18,1));
        System.out.println(getExponent(128,4));

//        System.out.println(calculatePower(3,4));


    }

    private static int getExponent ( int n, int p ) {
       int count = 0;
        if ( p <=1 ) {
            return -1;
        }

         while ( n % p ==0 ) {
             n /= p;
             count++;
         }

        return count;
    }

    private static int calculatePower ( int n, int exponentCount ) {
        int total = 1;
        for ( int i = 0; i < exponentCount; i++ ) {
             total *= n;
        }

        return total;
    }

}
