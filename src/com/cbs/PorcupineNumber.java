package com.cbs;

/**
 * A prime number is an integer that is divisible only by 1 and itself.
 * A porcupine number is a prime number whose last digit is 9 and the next prime number that follows it also ends with the digit 9.
 * For example 139 is a porcupine number because:
 * a. it is prime
 * b. it ends in a 9
 * c. The next prime number after it is 149 which also ends in 9.
 * Note that 140, 141, 142, 143, 144, 145, 146, 147 and 148 are not prime so 149 is the next prime number after 139.
 * Write a method named findPorcupineNumber which takes an integer argument n and returns the first porcupine number that is greater than n.
 * So findPorcupineNumber(0) would return 139 (because 139 happens to be the first porcupine number) and so would findPorcupineNumber(138).
 * But findPorcupineNumber(139) would return 409 which is the second porcupine number.
 * The function signature is 	int findPorcupineNumber(int n)
 * <p>
 * You may assume that a porcupine number greater than n exists.
 *
 * @Author
 * @Created 2024/01/10
 * @Name MIU
 */

public class PorcupineNumber {

    public static void main ( String[] args ) {
        System.out.println(findPorcupineNumber(0));
        System.out.println(findPorcupineNumber(139));
        System.out.println(findPorcupineNumber(191));
        System.out.println(findPorcupineNumber(999));
        System.out.println(findPorcupineNumber(2000));

    }

    private static int findPorcupineNumber ( int startNumber ) {

        boolean roundNext = true;
        int porcupineNumber = 0;

        while ( roundNext ) {
            startNumber++;
            if ( isPrime(startNumber) ) {
                int previousPrime = startNumber;
                int nextPrime = nextPrimeV2(startNumber + 1);
                if ( previousPrime % 10 == 9 && nextPrime % 10 == 9 ) {
                    porcupineNumber = previousPrime;
                    break;
                }
            }
        }
        return porcupineNumber;
    }

    static int nextPrime ( int n ) {
        boolean nextRound = true;

        while ( nextRound ) {
            if ( isPrime(n) ) {
                break;
            }
            n++;
        }

        return n;
    }

    static int nextPrimeV2 ( int n ) {
      while ( !isPrime(n) ){
          n++;
      }
      return n;
    }

    static boolean isPrime ( int n ) {
        if ( n <= 1 ) {
            return false;
        }

        for ( int i = 2; i < n; i++ ) {
            if ( n % i == 0 ) {
                return false;
            }
        }

        return true;
    }

}
