package com.cbs;

/**
 * An array can hold the digits of a number.
 * For example the digits of the number 32053 are stored in the array {3, 2, 0, 5, 3}.
 * Write a method call repsEqual that takes an array and an integer and returns 1 if the array contains
 * only the digits of the number in the same order that they appear in the number. Otherwise it returns 0.
 * If you are programming in Java or C#, the function prototype is
 * int repsEqual(int[ ] a, int n)
 * If you are programming in C++ or C, the function prototype is
 * int repsEqual(int a[ ], int len, int n) where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/01/11
 * @Name MIU
 */

public class RepsEqual {

    public static void main ( String[] args ) {
        int[] arr = {3, 2, 0, 5, 3};
        int[] arr1 = {3, 2, 0, 5};
        int[] arr2 = {3, 2, 0, 5, 3, 4};
        int[] arr3 = {2, 3, 0, 5, 3};
        int[] arr4 = {9, 3, 1, 1, 2};
        int[] arr5 = {0,0, 3, 2, 0, 5, 3};

        System.out.println(isRepsEqual (arr,32053));
        System.out.println(isRepsEqual (arr1,32053));
        System.out.println(isRepsEqual (arr2,32053));
        System.out.println(isRepsEqual (arr3,32053));
        System.out.println(isRepsEqual (arr4,32053));
        System.out.println(isRepsEqual (arr5,32053));
    }

    private static int isRepsEqual ( int[] arr, int number ) {
        int isReps = 1;
        for ( int i = arr.length-1; i > 0; i-- ) {
              int nextNumber = number % 10;
              number = number / 10 ;
              if ( arr[i] != nextNumber ){
                  isReps = 0;
                  break;
              }
        }

        return isReps;
    }

}
