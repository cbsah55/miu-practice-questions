package com.cbs;

/**
 * @Author
 * @Created 2024/01/04
 * @Name MIU
 */

public class NUpCount {

    public static void main ( String[] args ) {
        int upCount = 5;
        int[] arr = {2, 3, 1, -6, 8, -3, -1, 2};
        int[] arr1 = {6, 3, 1};
        int[] arr2 = {1, 2, -1, 5, 3, 2, -3};
        System.out.println(nUpCount(arr, upCount));
        System.out.println(nUpCount(arr1, 6));
        System.out.println(nUpCount(arr2, 20));
    }

    private static int nUpCount ( int[] arr, int upCount ) {
        int partialSum = 0;
        int upCountFreq = 0;
        for ( int i = 0; i < arr.length; i++ ) {
            int previousPartialSum = partialSum;
            partialSum += arr[i];
            if ( previousPartialSum <= upCount && partialSum > upCount ) {
                upCountFreq++;
            }

        }
        return upCountFreq;
    }

}
