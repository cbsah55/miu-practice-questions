package com.cbs;

/**
 * Write a function named maxOccurDigit that returns the digit that occur the most.
 * If  there is no such digit, it will return -1.
 * For example maxOccurDigit(327277) would return  7 because 7 occurs three times in the number
 * and all other digits occur less than three  times.
 * Other examples:
 * maxOccurDigit(33331) returns 3
 * maxOccurDigit(32326) returns -1
 * maxOccurDigit(5) returns 5
 * maxOccurDigit(-9895) returns 9
 * The function signature is maxOccurDigit(int n)
 *
 * @Author
 * @Created 2024/02/08
 * @Name MIU
 */

public class MaxOccurDigit {

    public static void main ( String[] args ) {
        System.out.println(maxOccurDigit(327277));
        System.out.println(maxOccurDigit(33331));
        System.out.println(maxOccurDigit(32326));
        System.out.println(maxOccurDigit(5));
        System.out.println(maxOccurDigit(-9895));

        System.out.println("-------------------------------");

    }

    private static int maxOccurDigit ( int n ) {
        int[] arr = new int[10];
        int max = 0;
        int maxDigitCount= -1;
        boolean isUnique = true;

        if ( n < 0 ){
            n = n * -1;
        }
        while ( n > 0 ){
            int lastDigit = n % 10;
            arr[lastDigit]++;
            n = n /10;
        }

        for ( int i = 0; i < arr.length; i++ ) {
             if ( arr[i] > max ){
                 max = arr[i];
                  maxDigitCount = i;
                  isUnique = true;
             } else if ( arr[i] == max ) {
                 isUnique = false;
             }

             }

        return max ==0 || !isUnique ? -1 : maxDigitCount;

        }
}
