package com.cbs;

import java.util.Arrays;

/**
 * @Author
 * @Created 2024/01/22
 * @Name MIU
 */

public class RotateArray {

    public static void main ( String[] args ) {
        int[] nums = {1,2,3,4,5,6,7} ;

        System.out.println("Array : " + Arrays.toString(nums) + " Rotated ->"+Arrays.toString(rotate(nums, 3)));
        System.out.println("Array : " + Arrays.toString(nums) + " Rotated ->"+Arrays.toString(rotatev2(nums, 2)));


    }

    public static int[] rotate(int[] nums, int k) {
        int[] newArr = new int[nums.length];
        for(int i = 1; i<=k;i++){
            int temp = nums[nums.length-1];
            newArr[0] = temp;

            for( int j = 1; j< nums.length; j++){
                newArr[j] = nums[j-1];
            }

            nums = Arrays.copyOf(newArr,newArr.length);

        }
        return nums;

    }

    public static int[]  rotatev2(int[] arr,int k) {
        int len = arr.length;
        k = k % len;  // If k is greater than the length of the array
        for (int i = 0; i < k; i++) {
            int last = arr[len - 1];
            for (int j = len - 1; j > 0; j--) {
                arr[j] = arr[j - 1];
            }
            arr[0] = last;
        }

        return arr;
    }




}
