package com.cbs;

/**
 * An Sub array is defined to be an array in which each element is greater than sum of all  elements afterthat.
 * See examples below:
 * {13, 6, 3, 2} is a Sub array.
 * Note that 13 > 2 + 3 + 6, 6 > 3 + 2, 3 > 2.
 * {11, 5, 3, 2} is a NOT a Sub array.
 * Note that 5 is not greater than 3 + 2.
 * Write a function named isSub that returns 1 if its array argument is a Sub array, otherwise  it returns 0.
 *
 * @Author
 * @Created 2024/02/08
 * @Name MIU
 */

public class SubArray {

    public static void main ( String[] args ) {
        int[] arr = {13, 6, 3, 2};
        int[] arr1 = {11, 5, 3, 2};



        System.out.println(isSub(arr));
        System.out.println(isSub(arr1));

    }

    private static int isSub ( int[] arr ) {
        boolean isSub = true;

        for ( int i = 0; i < arr.length; i++ ) {
            int n = arr[i];
            int sum = 0;
            for ( int j = i+1; j < arr.length; j++ ) {
                sum += arr[j];
            }

            if ( n <= sum ){
                isSub = false;
                break;
            }
        }



        return isSub ? 1 : 0;
    }
}
