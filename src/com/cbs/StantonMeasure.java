package com.cbs;

/**
 * The Stanton measure of an array is computed as follows.
 * Count the number of 1s in the array. Let this count be n.
 * The Stanton measure is the number of times that n appears in the array.
 * For example, the Stanton measure of {1, 4, 3, 2, 1, 2, 3, 2} is 3 because 1 occurs 2 times in the array and 2 occurs 3 times.
 * Write a function named stantonMeasure that returns the Stanton measure of its array argument.
 * If you are  programming in Java or C#, the function prototype is
 * int stantonMeasure(int[ ] a)
 * If you are programming in C++ or C, the function prototype is
 * int stantonMeasure(int a[ ], int len)
 * where len is the number of elements in the array.
 *
 * @Author
 * @Created 2024/01/11
 * @Name MIU
 */

public class StantonMeasure {

    public static void main ( String[] args ) {
        int[] arr = {1};
        int[] arr1 = {0};
        int[] arr2 = {3, 1, 1, 4};
        int[] arr3 = {1, 3, 1, 1, 3, 3, 2, 3, 3, 3, 4};
        int[] arr4 = {};
        int[] arr7 = {1,3,1,1,3,3,2,3,3,3,4,6,5,2,6};



        System.out.println(stantonMeasure(arr));
        System.out.println(stantonMeasure(arr1));
        System.out.println(stantonMeasure(arr2));
        System.out.println(stantonMeasure(arr3));
        System.out.println(stantonMeasure(arr4));
        System.out.println(stantonMeasure(arr7));

    }

    private static int stantonMeasure ( int[] arr ) {
        int onesCount = 0;
        for ( int i = 0; i < arr.length; i++ ) {
            if ( arr[i] == 1 ) {
                onesCount++;
            }
        }


        int nextStantonNumber = onesCount;
        int stantonCount = 0;

        while ( true ) {
            for ( int i = 0; i < arr.length; i++ ) {
                if ( nextStantonNumber == arr[i] ) {
                    stantonCount++;
                }
            }
            nextStantonNumber = stantonCount;
            if ( nextStantonNumber == stantonCount ) break;
            stantonCount = 0;
        }

        return stantonCount;
    }


    public static int stantonMeasureV2 ( int[] a ) {
        int count = 0;
        for ( int i = 0; i < a.length; i++ ) {
            //first finding the counts of 1
            if ( a[i] == 1 ) {
                count++;
            }

        }
        int rtnstantonMeasure = 0;
        for ( int j = 0; j < a.length; j++ ) {
            if ( count == a[j] ) {
                rtnstantonMeasure++;
            }
        }
        return rtnstantonMeasure;

    }

}
