package com.cbs;

/**
 * Define a square pair to be the tuple <x, y> where x and y are positive, non-zero integers,
 * x<y and x + y is a perfect square.
 * A perfect square is an integer whose square root is also an integer,
 * e.g. 4, 9, 16 are perfect squares but 3, 10 and 17 are not.
 * Write a function named countSquarePairs that takes an array and
 * returns the number of square pairs that can be constructed from the elements in the array.
 * For example, if the array is {11, 5, 4, 20} the function would return 3 because
 * the only square pairs that can be constructed from those numbers are <5, 11>,  <5, 20> and <4, 5>.
 * You may assume that there exists a function named isPerfectSquare that returns 1 if its argument is a perfect square and 0 otherwise.
 * E.G., isPerfectSquare(4) returns 1 and isPerfectSquare(8) returns 0.
 * If you are programming in Java or C#, the function signature is
 * int countSquarePairs(int[ ] a)
 * If you are programming in C++ or C, the function signature is
 * int countSquarePairs(int a[ ], int len)  where len is the number of elements in the array.
 * You may assume that there are no duplicate values in the array, i.e, you don't have to deal with an array like {2, 7, 2, 2}.
 *
 * @Author
 * @Created 2024/01/08
 * @Name MIU
 */

public class SquarePair {

    public static void main ( String[] args ) {
        int[] arr = {11, 5, 4, 20} ;
        int[] arr1 = {9, 0, 2, -5, 7};
        int[] arr2 = {9};


        System.out.println(countSquarePairs(arr));
        System.out.println(countSquarePairs(arr1));
        System.out.println(countSquarePairs(arr2));
    }

    static int countSquarePairs(int[] arr){
        int countSquarePair = 0;
        for ( int i = 0; i < arr.length; i++ ) {
            for ( int j = 0; j < arr.length; j++ ) {
                if ( arr[i] < arr[j] && arr[i] > 0 ){
                    int sumOfPair =  arr[i] + arr[j];
                     if ( isPerfectSquare(sumOfPair) ){
                         countSquarePair++;
                     }
                }
            }
        }


        return countSquarePair;
    }

    static boolean isPerfectSquare(int number){
        int squareRoot = (int) Math.sqrt(number);

        return squareRoot * squareRoot == number;
    }

    static boolean isPerfectSquareV2(int number) {
        boolean isPerfectSquare = false;
        for (int i = 0; i < number/2; i++) {
            if (i * i == number) {
                isPerfectSquare = true;
                break;
            }
        }
        return isPerfectSquare;
    }

}
